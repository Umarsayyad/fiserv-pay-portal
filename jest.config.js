module.exports = {
  preset: "@vue/cli-plugin-unit-jest",
  collectCoverageFrom: [
    "src/**/*.{js,vue}",
    "!**/main.js",
    "!**/index.js",
    "!**/routes.js",
    "!**/vuetify.js",
    "!**/dateTimeFormats.js",
    "!**/node_modules/**",
    "!**/src/locales/**",
    "!**/src/api/clients/**",
  ],
  setupFiles: ["./jest.setup.js", "jest-canvas-mock"],
  transform: {
    "^.+\\.svg$": "<rootDir>/tests/svg-transform.js",
  },
};
