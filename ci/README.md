## DDP WebApp CI CD


This directory contains templates to manage CI CD for DDP Web Application. It includes the following files.

- **`ddp-ui-pipeline.yaml`** - Pipeline script for `ddp-aws-webapp-build` pipeline in [Non Prod OMA](https://concourse-oma-nonprod.1dc.com/).
- *TBD:: Release pipelines for deployment to higher environments.*

**Requirements/guidelines to configure CI/CD for AWS cloud applications:**

1. Establish a service account in AWS with privileges; this account will be used to configure pipelines to perform deployments. Sample [JIRA task](https://escmjira.1dc.com/browse/CLOUDOPS-2629) raised to CloudOps team for creating service account.
1. To create/update concourse pipelines, request user to concourse for appropriate concourse env via JIRA request. Sample [JIRA Task](https://escmjira.1dc.com/projects/PCFMAN/issues/PCFMAN-600?filter=allopenissues)
1. Grant access to git service account `ucom_cf_git` to repo
1. Fiserv CI CD [documentation](https://fdcp-wiki2.apps.us-oma1-np2.1dc.com/cf/CI%20-%20CD/)

**Assumptions & notes:**
1. DDP non prod pipelines are configured in Non Prod OMA councourse env under `USA.MRCH.APP.DDP` team.  
1. Pipelines are built using docker images for: 
    * Python 3.8, AWS CLI & SAM CLI 
    * NodeJS v15
    * GIT  
    * Fortify client
1. Fortify scans are performed on code in - ./src/frontend dir
1. Fortify Scan project details: 
    * NAME = "UAID-06531"
    * VERSION = "ddp-recipient-portal"
1. Pipeline `build-and-deploy` job performs the following steps to deploy:
    * Compile UI code in `/src/frontend` by executing the following commands to generate artifacts in `dist`
        * `npm install` 
        * `npm run lint -- --no-fix --max-warnings 0`
        * `npm run test:unit ` 
        * `NODE_ENV=production npm run build -- --mode<env>`
    
    * Empty contents in AWS S3 bucket for a given environment
    * Copy contents from artifacts folder - `.src/frontend/dist` to AWS S3 bucket
    * Invalidate Cloudfront distribution cache

**DDP branching strategy, continuous integration & delivery**
Reference: [Branching model](https://escmconfluence.1dc.com/pages/viewpage.action?pageId=492155225#AAMP-uComCI/CDImplementation-BranchingModel) 
1. Dev team develops on feature/bug fix branch, perform unit testing
2. Dev team merge code from feature --> dev branch\
    .

    ![devpipeline](./devpipelineUI.png)
3. Dev team merge code from dev branch --> integration branch for QA testing\
   Flow:

   ![qapipeline](./qapipelineUI.png)

4. QA team perform testing in QA environment 
    * provide QA signoff
5. Dev team create release version\
    Flow

    ![relpipeline](./relpipeline.png)
6. *TBD:Release/DevOps team - Trigger pipleine job*
    * *Checkout and deploy from intergration branch based on release version tag*

