module.exports = {
  lintOnSave: false,

  devServer: {
    public: "localhost:8080",
    hot: false,
    proxy: {
      "/auth-api": {
        target: "https://qa.digitalpayouts.com/v1",
        pathRewrite: { "^/auth-api": "" },
      },
      "/ddp-api": {
        target: "https://qa.api.fiservapps.com/ddp/portal/v1",
        pathRewrite: { "^/ddp-api": "" },
      },
    },
  },

  transpileDependencies: ["vuetify"],
  publicPath: process.env.NODE_ENV === "production" ? "/portal/" : "/",

  chainWebpack: (config) => {
    config.plugin("html").tap((args) => {
      args[0].title = "Digital Payouts";
      return args;
    });

    const svgRule = config.module.rule("svg");

    svgRule.uses.clear();

    svgRule
      .use("babel-loader")
      .loader("babel-loader")
      .end()
      .use("vue-svg-loader")
      .loader("vue-svg-loader");
  },

  pluginOptions: {
    i18n: {
      locale: "en-US",
      fallbackLocale: "en-US",
      localeDir: "locales",
      enableInSFC: false,
    },
  },
};
