import Amplify from "aws-amplify";

function configure() {
  Amplify.configure({
    Auth: {
      region: process.env.VUE_APP_COGNITO_REGION,
      userPoolId: process.env.VUE_APP_COGNITO_USER_POOL_ID,
      userPoolWebClientId: process.env.VUE_APP_COGNITO_CLIENT_ID,
      mandatorySignIn: false,
      storage: sessionStorage,
    },
  });
}

export default {
  configure,
};
