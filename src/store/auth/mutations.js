import { SET_REDIRECT, SET_USER } from "@/store/mutation-types";

export default {
  [SET_USER](state, user) {
    state.user = user;
  },

  [SET_REDIRECT](state, route) {
    state.redirect = route;
  },
};
