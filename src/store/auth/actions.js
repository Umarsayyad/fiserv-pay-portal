import {
  COMPLETE_NEW_PASSWORD,
  CONFIRM_SIGN_IN,
  CURRENT_AUTHENTICATED_USER,
  FORGOT_PASSWORD,
  FORGOT_PASSWORD_SUBMIT,
  DISABLE_SMS_MFA,
  ENABLE_SMS_MFA,
  SIGN_IN,
  SIGN_OUT,
} from "@/store/action-types";
import { SET_REDIRECT, SET_USER } from "@/store/mutation-types";
import { Auth } from "aws-amplify";

export default {
  async [SIGN_IN]({ commit }, creds) {
    const user = await Auth.signIn(creds.username, creds.password);
    commit(SET_USER, user);

    return user;
  },

  async [CONFIRM_SIGN_IN]({ commit, state }, code) {
    const user = await Auth.confirmSignIn(state.user, code);
    commit(SET_USER, user);

    return user;
  },

  async [COMPLETE_NEW_PASSWORD]({ commit, state }, newPassword) {
    const user = await Auth.completeNewPassword(state.user, newPassword);
    commit(SET_USER, user);

    return user;
  },

  async [SIGN_OUT]({ commit }) {
    await Auth.signOut();
    commit(SET_USER, null);
    commit(SET_REDIRECT, { name: "home" });
  },

  async [CURRENT_AUTHENTICATED_USER]({ commit }) {
    const user = await Auth.currentAuthenticatedUser();
    commit(SET_USER, user);

    return user;
  },

  [FORGOT_PASSWORD](_, username) {
    return Auth.forgotPassword(username);
  },
  [FORGOT_PASSWORD_SUBMIT](_, creds) {
    return Auth.forgotPasswordSubmit(
      creds.username,
      creds.otp,
      creds.newPassword
    );
  },

  [ENABLE_SMS_MFA]({ state }) {
    return Auth.setPreferredMFA(state.user, "SMS");
  },

  [DISABLE_SMS_MFA]({ state }) {
    return Auth.setPreferredMFA(state.user, "NOMFA");
  },
};
