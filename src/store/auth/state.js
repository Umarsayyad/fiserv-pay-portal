import { mockUser } from "@/store/auth/mock-data";

const state = () => {
  return {
    user: mockUser,
    redirect: { name: "home" },
  };
};

export default state;
