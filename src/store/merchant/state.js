export default () => {
  return {
    merchants: {
      lyft: {
        color: "#ff008f",
      },
      statefarm: {
        color: "#fc2929",
      },
    },
  };
};
