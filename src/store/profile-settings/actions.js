import { FETCH_USER_SETTINGS } from "../action-types";

export default {
  [FETCH_USER_SETTINGS]({ state }) {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(state.profile);
      }, 1000);
    });
  },
};
