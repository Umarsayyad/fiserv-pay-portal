import { mockUser } from "@/store/profile-settings/mock-data";

const state = () => {
  return {
    profile: mockUser,
  };
};

export default state;
