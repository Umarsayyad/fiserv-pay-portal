import Vue from "vue";
import Vuex from "vuex";
import app from "./app";
import auth from "./auth";
import merchant from "./merchant";
import payouts from "./payouts";
import paymentMethods from "./payment-methods";
import profile from "./profile-settings";

Vue.use(Vuex);

const modules = {
  app,
  auth,
  merchant,
  payouts,
  paymentMethods,
  profile,
};

const store = new Vuex.Store({ modules });

export default store;
