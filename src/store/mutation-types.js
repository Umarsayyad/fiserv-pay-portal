// app
export const SET_SHOW_FOOTER = "SET_SHOW_FOOTER";
export const SET_SPLIT_BACKGROUND = "SET_SPLIT_BACKGROUND";
export const CLEAR_APP_ERRORS = "CLEAR_APP_ERRORS";
export const PUSH_APP_ERROR = "PUSH_APP_ERROR";

// auth
export const SET_USER = "SET_USER";
export const SET_REDIRECT = "SET_REDIRECT";

// payouts
export const SET_PAYOUTS = "SET_PAYOUTS";
export const SET_PAYOUT = "SET_PAYOUT";

// payout methods
export const SET_DIALOG_CONTENT = "SET_DIALOG_CONTENT";
