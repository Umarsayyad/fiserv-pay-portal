import { add, format, sub } from "date-fns";

const now = new Date();
const dateFmt = "MM/dd/yyyy";
const nowFormatted = format(now, dateFmt);

const tomorrow = add(now, { days: 1 });
const tomorrowFormatted = format(tomorrow, dateFmt);

export const mockAvailablePayouts = {
  transactions: [
    {
      transactionId: "TID-f142f499-d86f-4364-920e-7d2de3900b9c",
      created: 1618592828,
      merchantTransactionId: "TransactionID-1991309",
      paymentReferenceNumber: "1234567904",
      applyDate: "04/15/2021",
      transactionDate: "04/15/2021",
      updateDate: "04/15/2021",
      expiryDate: tomorrowFormatted,
      recipient: [
        {
          merchantCustomerId: "merchantCustomerId-29764558",
          recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
          payments: {
            paymentId: "24e2e932-5f14-44c0-ace8-a03ea5260349",
            amount: {
              total: 200,
              currency: "USD",
            },
            fee: {
              total: 0,
              currency: "USD",
            },
            paymentType: "Claims",
            paymentStatus: "DP",
          },
          doingBusinessAs: "Lyft",
          firstName: "TYRION",
          lastName: "LANNISTER",
          groupName: "lyft",
        },
      ],
      transactionStatus: "PD",
      totalTransactionAmount: {
        total: 200,
        currency: "USD",
      },
    },
    {
      transactionId: "TID-ee1f4255-3c2e-45b6-8028-5c2b587fde4c",
      created: 1618592828,
      merchantTransactionId: "TransactionID-1991310",
      paymentReferenceNumber: "1234567904",
      applyDate: "04/16/2021",
      transactionDate: "04/16/2021",
      updateDate: "04/16/2021",
      expiryDate: nowFormatted,
      recipient: [
        {
          merchantCustomerId: "merchantCustomerId-29764558",
          recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
          payments: {
            paymentId: "47884808-337f-4a19-83d1-5cb6a16189a7",
            amount: {
              total: 201,
              currency: "USD",
            },
            fee: {
              total: 0,
              currency: "USD",
            },
            paymentType: "Claims",
            paymentStatus: "DP",
          },
          doingBusinessAs: "State Farm",
          firstName: "TYRION",
          lastName: "LANNISTER",
          groupName: "statefarm",
        },
      ],
      transactionStatus: "PD",
      totalTransactionAmount: {
        total: 201,
        currency: "USD",
      },
    },
    {
      transactionId: "TID-094d7fc0-2f81-4fb7-8ca7-f2683c382e45",
      created: 1618592828,
      merchantTransactionId: "merchantTransactionId-098699",
      applyDate: "04/08/2021",
      transactionDate: "04/08/2021",
      updateDate: "04/11/2021",
      expiryDate: tomorrowFormatted,
      recipient: [
        {
          merchantCustomerId: "merchantCustomerId-29764558",
          recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
          payments: {
            paymentId: "fa81b013-aed7-490d-950c-3290139ecbdd",
            amount: {
              total: 100,
              currency: "USD",
            },
            fee: {
              total: 0,
              currency: "USD",
            },
            paymentType: "Claims",
            paymentStatus: "PE",
          },
          doingBusinessAs: "Lyft",
          firstName: "TYRION",
          lastName: "LANNISTER",
          groupName: "lyft",
        },
      ],
      transactionStatus: "PD",
      totalTransactionAmount: {
        total: 100,
        currency: "USD",
      },
    },
    {
      transactionId: "TID-eb59f713-edf4-48ba-8f52-1379df1db9b3",
      created: 1618592828,
      merchantTransactionId: "merchantTransactionId-098698",
      applyDate: "04/08/2021",
      transactionDate: "04/08/2021",
      updateDate: "04/11/2021",
      expiryDate: tomorrowFormatted,
      recipient: [
        {
          merchantCustomerId: "merchantCustomerId-29764558",
          recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
          payments: {
            paymentId: "e35c34d2-e851-47db-befd-deb37f53041e",
            amount: {
              total: 100,
              currency: "USD",
            },
            fee: {
              total: 0,
              currency: "USD",
            },
            paymentType: "Claims",
            paymentStatus: "PE",
          },
          doingBusinessAs: "State Farm",
          firstName: "TYRION",
          lastName: "LANNISTER",
          groupName: "statefarm",
        },
      ],
      transactionStatus: "PD",
      totalTransactionAmount: {
        total: 100,
        currency: "USD",
      },
    },
    {
      transactionId: "TID-094d7fc0-2f81-4fb7-8ca7-f2683c382e45",
      created: 1618592828,
      merchantTransactionId: "merchantTransactionId-098699",
      applyDate: "04/08/2021",
      transactionDate: "04/08/2021",
      updateDate: "04/11/2021",
      expiryDate: tomorrowFormatted,
      recipient: [
        {
          merchantCustomerId: "merchantCustomerId-29764558",
          recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
          payments: {
            paymentId: "fa81b013-aed7-490d-950c-3290139ecbdd",
            amount: {
              total: 100,
              currency: "USD",
            },
            fee: {
              total: 0,
              currency: "USD",
            },
            paymentType: "Claims",
            paymentStatus: "PE",
          },
          doingBusinessAs: "State Farm",
          firstName: "TYRION",
          lastName: "LANNISTER",
          groupName: "statefarm",
        },
      ],
      transactionStatus: "PD",
      totalTransactionAmount: {
        total: 100,
        currency: "USD",
      },
    },
  ],
  pages: {
    firstPage: true,
    lastPage: true,
    currentPage: 1,
    currentPageRecords: 5,
    totalPages: 1,
    totalRecords: 5,
    sortOrder: "desc",
  },
};

export const mockPayouts = {
  transactions: [
    {
      transactionId: "TID-ee1f4255-3c2e-45b6-8028-5c2b587fde4c",
      created: 1618592828,
      merchantTransactionId: "TransactionID-1991310",
      paymentReferenceNumber: "1234567904",
      applyDate: "04/16/2021",
      transactionDate: "04/16/2021",
      updateDate: "04/16/2021",
      recipient: [
        {
          merchantCustomerId: "merchantCustomerId-29764558",
          recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
          payments: {
            paymentId: "47884808-337f-4a19-83d1-5cb6a16189a7",
            amount: {
              total: 201,
              currency: "USD",
            },
            fee: {
              total: 0,
              currency: "USD",
            },
            paymentType: "Claims",
            paymentStatus: "DP",
          },
          doingBusinessAs: "State Farm",
          firstName: "TYRION",
          lastName: "LANNISTER",
          groupName: "statefarm",
        },
      ],
      transactionStatus: "PD",
      totalTransactionAmount: {
        total: 201,
        currency: "USD",
      },
    },
    {
      transactionId: "TID-f142f499-d86f-4364-920e-7d2de3900b9c",
      created: 1618592828,
      merchantTransactionId: "TransactionID-1991309",
      paymentReferenceNumber: "1234567904",
      applyDate: "04/15/2021",
      transactionDate: "04/15/2021",
      updateDate: "04/15/2021",
      recipient: [
        {
          merchantCustomerId: "merchantCustomerId-29764558",
          recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
          payments: {
            paymentId: "24e2e932-5f14-44c0-ace8-a03ea5260349",
            amount: {
              total: 200,
              currency: "USD",
            },
            fee: {
              total: 0,
              currency: "USD",
            },
            paymentType: "Claims",
            paymentStatus: "DP",
          },
          doingBusinessAs: "State Farm",
          firstName: "TYRION",
          lastName: "LANNISTER",
          groupName: "statefarm",
        },
      ],
      transactionStatus: "PD",
      totalTransactionAmount: {
        total: 200,
        currency: "USD",
      },
    },
    {
      transactionId: "TID-094d7fc0-2f81-4fb7-8ca7-f2683c382e45",
      created: 1618592828,
      merchantTransactionId: "merchantTransactionId-098699",
      applyDate: "04/08/2021",
      transactionDate: "04/08/2021",
      updateDate: "04/11/2021",
      recipient: [
        {
          merchantCustomerId: "merchantCustomerId-29764558",
          recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
          payments: {
            paymentId: "fa81b013-aed7-490d-950c-3290139ecbdd",
            amount: {
              total: 100,
              currency: "USD",
            },
            fee: {
              total: 0,
              currency: "USD",
            },
            paymentType: "Claims",
            paymentStatus: "PE",
          },
          doingBusinessAs: "State Farm",
          firstName: "TYRION",
          lastName: "LANNISTER",
          groupName: "statefarm",
        },
      ],
      transactionStatus: "TE",
      totalTransactionAmount: {
        total: 100,
        currency: "USD",
      },
    },
    {
      transactionId: "TID-eb59f713-edf4-48ba-8f52-1379df1db9b3",
      created: 1618592828,
      merchantTransactionId: "merchantTransactionId-098698",
      applyDate: "04/08/2021",
      transactionDate: "04/08/2021",
      updateDate: "04/11/2021",
      recipient: [
        {
          merchantCustomerId: "merchantCustomerId-29764558",
          recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
          payments: {
            paymentId: "e35c34d2-e851-47db-befd-deb37f53041e",
            amount: {
              total: 100,
              currency: "USD",
            },
            fee: {
              total: 0,
              currency: "USD",
            },
            paymentType: "Claims",
            paymentStatus: "PE",
          },
          doingBusinessAs: "State Farm",
          firstName: "TYRION",
          lastName: "LANNISTER",
          groupName: "statefarm",
        },
      ],
      transactionStatus: "TE",
      totalTransactionAmount: {
        total: 100,
        currency: "USD",
      },
    },
  ],
  pages: {
    firstPage: true,
    lastPage: true,
    currentPage: 1,
    currentPageRecords: 4,
    totalPages: 1,
    totalRecords: 4,
    sortOrder: "desc",
  },
};

export const mockTotalPayoutAmountsByMerchant = [
  {
    groupName: "statefarm",
    doingBusinessAs: "State Farm",
    totalPayoutsAmount: {
      total: 1000,
      currency: "USD",
    },
  },
  {
    groupName: "lyft",
    doingBusinessAs: "Lyft",
    totalPayoutsAmount: {
      total: 500,
      currency: "USD",
    },
  },
];

export const mockPayoutHistory = {
  transactions: [
    {
      transactionId: "TID-ee1f4255-3c2e-45b6-8028-5c2b587fde4c",
      created: 1618592828,
      merchantTransactionId: "TransactionID-1991310",
      paymentReferenceNumber: "1234567904",
      applyDate: nowFormatted,
      transactionDate: nowFormatted,
      updateDate: nowFormatted,
      recipient: [
        {
          merchantCustomerId: "merchantCustomerId-29764558",
          recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
          payments: {
            paymentId: "47884808-337f-4a19-83d1-5cb6a16189a7",
            amount: {
              total: 201,
              currency: "USD",
            },
            fee: {
              total: 0,
              currency: "USD",
            },
            paymentType: "Claims",
            paymentStatus: "DI",
          },
          doingBusinessAs: "Lyft",
          firstName: "TYRION",
          lastName: "LANNISTER",
          groupName: "lyft",
        },
      ],
      transactionStatus: "TC",
      totalTransactionAmount: {
        total: 201,
        currency: "USD",
      },
    },
    {
      transactionId: "TID-ee1f4255-3c2e-45b6-8028-5c2b587fde4c",
      created: 1618592828,
      merchantTransactionId: "TransactionID-1991310",
      paymentReferenceNumber: "1234567904",
      applyDate: format(sub(now, { days: 1 }), dateFmt),
      transactionDate: format(sub(now, { days: 1 }), dateFmt),
      updateDate: format(sub(now, { days: 1 }), dateFmt),
      recipient: [
        {
          merchantCustomerId: "merchantCustomerId-29764558",
          recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
          payments: {
            paymentId: "47884808-337f-4a19-83d1-5cb6a16189a7",
            amount: {
              total: 1001,
              currency: "USD",
            },
            fee: {
              total: 0,
              currency: "USD",
            },
            paymentType: "Claims",
            paymentStatus: "DA",
          },
          doingBusinessAs: "Lyft",
          firstName: "TYRION",
          lastName: "LANNISTER",
          groupName: "lyft",
        },
      ],
      transactionStatus: "TC",
      totalTransactionAmount: {
        total: 1001,
        currency: "USD",
      },
    },
    {
      transactionId: "TID-ee1f4255-3c2e-45b6-8028-5c2b587fde4c",
      created: 1618592828,
      merchantTransactionId: "TransactionID-1991310",
      paymentReferenceNumber: "1234567904",
      applyDate: format(sub(now, { days: 2 }), dateFmt),
      transactionDate: format(sub(now, { days: 2 }), dateFmt),
      updateDate: format(sub(now, { days: 2 }), dateFmt),
      recipient: [
        {
          merchantCustomerId: "merchantCustomerId-29764558",
          recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
          payments: {
            paymentId: "47884808-337f-4a19-83d1-5cb6a16189a7",
            amount: {
              total: 500,
              currency: "USD",
            },
            fee: {
              total: 0,
              currency: "USD",
            },
            paymentType: "Claims",
            paymentStatus: "DR",
          },
          doingBusinessAs: "State Farm",
          firstName: "TYRION",
          lastName: "LANNISTER",
          groupName: "statefarm",
        },
      ],
      transactionStatus: "TC",
      totalTransactionAmount: {
        total: 500,
        currency: "USD",
      },
    },
    {
      transactionId: "TID-ee1f4255-3c2e-45b6-8028-5c2b587fde4c",
      created: 1618592828,
      merchantTransactionId: "TransactionID-1991310",
      paymentReferenceNumber: "1234567904",
      applyDate: format(sub(now, { days: 3 }), dateFmt),
      transactionDate: format(sub(now, { days: 3 }), dateFmt),
      updateDate: format(sub(now, { days: 3 }), dateFmt),
      recipient: [
        {
          merchantCustomerId: "merchantCustomerId-29764558",
          recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
          payments: {
            paymentId: "47884808-337f-4a19-83d1-5cb6a16189a7",
            amount: {
              total: 50.14,
              currency: "USD",
            },
            fee: {
              total: 0,
              currency: "USD",
            },
            paymentType: "Claims",
            paymentStatus: "PC",
          },
          doingBusinessAs: "Lyft",
          firstName: "TYRION",
          lastName: "LANNISTER",
          groupName: "lyft",
        },
      ],
      transactionStatus: "TC",
      totalTransactionAmount: {
        total: 50.14,
        currency: "USD",
      },
    },
    {
      transactionId: "TID-ee1f4255-3c2e-45b6-8028-5c2b587fde4c",
      created: 1618592828,
      merchantTransactionId: "TransactionID-1991310",
      paymentReferenceNumber: "1234567904",
      applyDate: format(sub(now, { days: 4 }), dateFmt),
      transactionDate: format(sub(now, { days: 4 }), dateFmt),
      updateDate: format(sub(now, { days: 4 }), dateFmt),
      recipient: [
        {
          merchantCustomerId: "merchantCustomerId-29764558",
          recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
          payments: {
            paymentId: "47884808-337f-4a19-83d1-5cb6a16189a7",
            amount: {
              total: 10020,
              currency: "USD",
            },
            fee: {
              total: 0,
              currency: "USD",
            },
            paymentType: "Claims",
            paymentStatus: "PE",
          },
          doingBusinessAs: "State Farm",
          firstName: "TYRION",
          lastName: "LANNISTER",
          groupName: "statefarm",
        },
      ],
      transactionStatus: "TC",
      totalTransactionAmount: {
        total: 10020,
        currency: "USD",
      },
    },
    {
      transactionId: "TID-ee1f4255-3c2e-45b6-8028-5c2b587fde4c",
      created: 1618592828,
      merchantTransactionId: "TransactionID-1991310",
      paymentReferenceNumber: "1234567904",
      applyDate: format(sub(now, { days: 5 }), dateFmt),
      transactionDate: format(sub(now, { days: 5 }), dateFmt),
      updateDate: format(sub(now, { days: 5 }), dateFmt),
      recipient: [
        {
          merchantCustomerId: "merchantCustomerId-29764558",
          recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
          payments: {
            paymentId: "47884808-337f-4a19-83d1-5cb6a16189a7",
            amount: {
              total: 740,
              currency: "USD",
            },
            fee: {
              total: 0,
              currency: "USD",
            },
            paymentType: "Claims",
            paymentStatus: "AF",
          },
          doingBusinessAs: "State Farm",
          firstName: "TYRION",
          lastName: "LANNISTER",
          groupName: "statefarm",
        },
      ],
      transactionStatus: "TC",
      totalTransactionAmount: {
        total: 740,
        currency: "USD",
      },
    },
    {
      transactionId: "TID-ee1f4255-3c2e-45b6-8028-5c2b587fde4c",
      created: 1618592828,
      merchantTransactionId: "TransactionID-1991310",
      paymentReferenceNumber: "1234567904",
      applyDate: format(sub(now, { days: 6 }), dateFmt),
      transactionDate: format(sub(now, { days: 6 }), dateFmt),
      updateDate: format(sub(now, { days: 6 }), dateFmt),
      recipient: [
        {
          merchantCustomerId: "merchantCustomerId-29764558",
          recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
          payments: {
            paymentId: "47884808-337f-4a19-83d1-5cb6a16189a7",
            amount: {
              total: 35,
              currency: "USD",
            },
            fee: {
              total: 0,
              currency: "USD",
            },
            paymentType: "Claims",
            paymentStatus: "RE",
          },
          doingBusinessAs: "Lyft",
          firstName: "TYRION",
          lastName: "LANNISTER",
          groupName: "lyft",
        },
      ],
      transactionStatus: "TC",
      totalTransactionAmount: {
        total: 35,
        currency: "USD",
      },
    },
    {
      transactionId: "TID-ee1f4255-3c2e-45b6-8028-5c2b587fde4c",
      created: 1618592828,
      merchantTransactionId: "TransactionID-1991310",
      paymentReferenceNumber: "1234567904",
      applyDate: format(sub(now, { days: 7 }), dateFmt),
      transactionDate: format(sub(now, { days: 7 }), dateFmt),
      updateDate: format(sub(now, { days: 7 }), dateFmt),
      recipient: [
        {
          merchantCustomerId: "merchantCustomerId-29764558",
          recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
          payments: {
            paymentId: "47884808-337f-4a19-83d1-5cb6a16189a7",
            amount: {
              total: 890,
              currency: "USD",
            },
            fee: {
              total: 0,
              currency: "USD",
            },
            paymentType: "Claims",
            paymentStatus: "DC",
          },
          doingBusinessAs: "State Farm",
          firstName: "TYRION",
          lastName: "LANNISTER",
          groupName: "statefarm",
        },
      ],
      transactionStatus: "TC",
      totalTransactionAmount: {
        total: 890,
        currency: "USD",
      },
    },
    {
      transactionId: "TID-ee1f4255-3c2e-45b6-8028-5c2b587fde4c",
      created: 1618592828,
      merchantTransactionId: "TransactionID-1991310",
      paymentReferenceNumber: "1234567904",
      applyDate: format(sub(now, { days: 8 }), dateFmt),
      transactionDate: format(sub(now, { days: 8 }), dateFmt),
      updateDate: format(sub(now, { days: 8 }), dateFmt),
      recipient: [
        {
          merchantCustomerId: "merchantCustomerId-29764558",
          recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
          payments: {
            paymentId: "47884808-337f-4a19-83d1-5cb6a16189a7",
            amount: {
              total: 765,
              currency: "USD",
            },
            fee: {
              total: 0,
              currency: "USD",
            },
            paymentType: "Claims",
            paymentStatus: "RS",
          },
          doingBusinessAs: "State Farm",
          firstName: "TYRION",
          lastName: "LANNISTER",
          groupName: "statefarm",
        },
      ],
      transactionStatus: "TC",
      totalTransactionAmount: {
        total: 765,
        currency: "USD",
      },
    },
    {
      transactionId: "TID-ee1f4255-3c2e-45b6-8028-5c2b587fde4c",
      created: 1618592828,
      merchantTransactionId: "TransactionID-1991310",
      paymentReferenceNumber: "1234567904",
      applyDate: format(sub(now, { days: 9 }), dateFmt),
      transactionDate: format(sub(now, { days: 9 }), dateFmt),
      updateDate: format(sub(now, { days: 9 }), dateFmt),
      recipient: [
        {
          merchantCustomerId: "merchantCustomerId-29764558",
          recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
          payments: {
            paymentId: "47884808-337f-4a19-83d1-5cb6a16189a7",
            amount: {
              total: 453,
              currency: "USD",
            },
            fee: {
              total: 0,
              currency: "USD",
            },
            paymentType: "Claims",
            paymentStatus: "RP",
          },
          doingBusinessAs: "State Farm",
          firstName: "TYRION",
          lastName: "LANNISTER",
          groupName: "statefarm",
        },
      ],
      transactionStatus: "TC",
      totalTransactionAmount: {
        total: 453,
        currency: "USD",
      },
    },
    {
      transactionId: "TID-ee1f4255-3c2e-45b6-8028-5c2b587fde4c",
      created: 1618592828,
      merchantTransactionId: "TransactionID-1991310",
      paymentReferenceNumber: "1234567904",
      applyDate: format(sub(now, { days: 10 }), dateFmt),
      transactionDate: format(sub(now, { days: 10 }), dateFmt),
      updateDate: format(sub(now, { days: 10 }), dateFmt),
      recipient: [
        {
          merchantCustomerId: "merchantCustomerId-29764558",
          recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
          payments: {
            paymentId: "47884808-337f-4a19-83d1-5cb6a16189a7",
            amount: {
              total: 122,
              currency: "USD",
            },
            fee: {
              total: 0,
              currency: "USD",
            },
            paymentType: "Claims",
            paymentStatus: "RF",
          },
          doingBusinessAs: "Lyft",
          firstName: "TYRION",
          lastName: "LANNISTER",
          groupName: "lyft",
        },
      ],
      transactionStatus: "TC",
      totalTransactionAmount: {
        total: 122,
        currency: "USD",
      },
    },
  ],
  pages: {
    firstPage: true,
    lastPage: false,
    currentPage: 1,
    currentPageRecords: 10,
    totalPages: 2,
    totalRecords: 11,
    sortOrder: "desc",
  },
};
