export default {
  brand(state) {
    if (!state.payout) {
      return null;
    }

    const recipient = state.payout.recipient[0];
    return {
      groupName: recipient.groupName,
      displayName: recipient.doingBusinessAs,
    };
  },
  availablePayoutAmount(state) {
    const payoutAmount =
      state.availablePayouts.transactions &&
      state.availablePayouts.transactions.reduce(
        (sum, item) => sum + item.totalTransactionAmount.total,
        0
      );
    return payoutAmount | 0;
  },
};
