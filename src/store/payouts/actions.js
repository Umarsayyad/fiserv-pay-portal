import {
  FETCH_AVAILABLE_PAYOUTS,
  FETCH_PAYOUT,
  FETCH_PAYOUT_HISTORY,
  FETCH_PAYOUTS,
  FETCH_TOTAL_PAYOUT_AMOUNTS_BY_MERCHANT,
  REJECT_PAYOUT,
} from "@/store/action-types";
import { SET_PAYOUT, SET_PAYOUTS } from "@/store/mutation-types";

export default {
  [FETCH_PAYOUTS]({ state }, empty) {
    return new Promise((resolve) => {
      const payouts = empty ? { transactions: [] } : state.payouts;
      setTimeout(() => resolve(payouts), 1000);
    });
  },

  [FETCH_PAYOUT]({ commit, state }, id) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const payouts = state.payouts.transactions.filter(
          (tx) => tx.transactionId === id
        );

        if (!payouts.length) {
          reject({ response: { status: 404 } });
          return;
        }

        const payout = payouts[0];
        commit(SET_PAYOUT, payout);
        resolve(payout);
      }, 1000);
    });
  },

  [REJECT_PAYOUT]({ commit, state }, description) {
    return new Promise((resolve) => {
      setTimeout(() => {
        const payout = {
          ...state.payout,
          transactionStatus: "TC",
          reason: description,
        };
        commit(SET_PAYOUT, payout);

        const payouts = state.payouts.transactions.map((p) =>
          p.transactionId === payout.transactionId ? payout : p
        );
        commit(SET_PAYOUTS, { ...state.payouts, transactions: payouts });

        resolve({
          transactionId: payout.transactionId,
          transactionStatus: "TC",
        });
      }, 1000);
    });
  },

  [FETCH_AVAILABLE_PAYOUTS]({ state }) {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(state.availablePayouts);
      }, 1000);
    });
  },

  [FETCH_TOTAL_PAYOUT_AMOUNTS_BY_MERCHANT]({ state }) {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(state.totalPayoutAmountsByMerchant);
      }, 1000);
    });
  },

  [FETCH_PAYOUT_HISTORY]({ state }, page) {
    return new Promise((resolve) => {
      setTimeout(() => {
        const history = state.history;
        const transactions = history.transactions.slice(
          page.page * page.size - page.size,
          page.page * page.size
        );
        const pages = {
          ...history.pages,
          firstPage: page.page === 1,
          lastPage:
            Math.ceil(history.pages.totalRecords / page.size) === page.page,
          currentPage: page.page,
          currentPageRecords: transactions.length,
        };
        resolve({ transactions, pages });
        // reject({ message: "Failed to retrieve payout history" });
      }, 1000);
    });
  },
};
