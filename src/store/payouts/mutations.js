import { SET_PAYOUT, SET_PAYOUTS } from "@/store/mutation-types";

export default {
  [SET_PAYOUTS](state, payouts) {
    state.payouts = payouts;
  },

  [SET_PAYOUT](state, payout) {
    state.payout = payout;
  },
};
