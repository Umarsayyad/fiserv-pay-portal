import state from "@/store/payouts/state";
import mutations from "@/store/payouts/mutations";
import actions from "@/store/payouts/actions";
import getters from "@/store/payouts/getters";

export default {
  state,
  mutations,
  actions,
  getters,
};
