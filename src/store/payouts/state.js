import {
  mockAvailablePayouts,
  mockPayoutHistory,
  mockPayouts,
  mockTotalPayoutAmountsByMerchant,
} from "@/store/payouts/mock-data";

const state = () => {
  return {
    payouts: mockPayouts,
    payout: null,
    availablePayouts: mockAvailablePayouts,
    totalPayoutAmountsByMerchant: mockTotalPayoutAmountsByMerchant,
    history: mockPayoutHistory,
  };
};

export default state;
