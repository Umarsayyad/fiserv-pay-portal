export const mockPaymentMethods = {
  accounts: [
    {
      type: "DEBIT",
      name: "CitiBank",
      credit: {
        cardNumber: "1234567890124455",
        nameOnCard: "Test Card",
        cardType: "VISA",
        billingAddress: {
          type: "work",
          streetAddress: "100 Universal City Plaza",
          locality: "Hollywood",
          region: "CA",
          postalCode: "91608",
          country: "USA",
          formatted: "100 Universal City Plaza\nHollywood, CA 91608 USA",
          primary: true,
        },
        expiryDate: {
          month: "01",
          year: "26",
        },
      },
    },
    {
      type: "ACH",
      name: "Bank of America",
      ach: {
        accountNumber: "123456789000",
        routingNumber: "283079227",
        type: "CHECKING",
      },
    },
    {
      type: "ACH",
      name: "Chase",
      ach: {
        accountNumber: "123456788230",
        routingNumber: "283079227",
        type: "CHECKING",
      },
    },
    {
      type: "ACH",
      name: "Wells Fargo",
      ach: {
        accountNumber: "123456783565",
        routingNumber: "283079227",
        type: "CHECKING",
      },
    },
    {
      type: "ACH",
      name: "Capital One",
      ach: {
        accountNumber: "123456781640",
        routingNumber: "283079227",
        type: "CHECKING",
      },
    },
  ],
};
