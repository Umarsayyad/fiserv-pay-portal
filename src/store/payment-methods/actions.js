import { FETCH_PAYMENT_METHODS } from "../action-types";

export default {
  [FETCH_PAYMENT_METHODS]({ state }) {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(state.paymentMethods.accounts);
      }, 1000);
    });
  },
};
