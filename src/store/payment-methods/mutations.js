import { SET_DIALOG_CONTENT } from "@/store/mutation-types";

export default {
  [SET_DIALOG_CONTENT](state, content) {
    state.dialogContent = content;
  },
};
