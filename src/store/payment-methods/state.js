import { mockPaymentMethods } from "@/store/payment-methods/mock-data";

const state = () => {
  return {
    paymentMethods: mockPaymentMethods,
    dialogContent: "new-payout-methods",
  };
};

export default state;
