import state from "./state";
import actions from "./actions";
import mutations from "@/store/payment-methods/mutations";

export default {
  state,
  actions,
  mutations,
};
