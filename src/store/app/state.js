const state = () => {
  return {
    showFooter: true,
    splitBackground: false,
    error: {
      show: false,
      values: [],
    },
  };
};

export default state;
