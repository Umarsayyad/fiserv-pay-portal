import {
  CLEAR_APP_ERRORS,
  PUSH_APP_ERROR,
  SET_SHOW_FOOTER,
  SET_SPLIT_BACKGROUND,
} from "@/store/mutation-types";

export default {
  [SET_SHOW_FOOTER](state, showFooter) {
    state.showFooter = showFooter;
  },

  [SET_SPLIT_BACKGROUND](state, splitBackground) {
    state.splitBackground = splitBackground;
  },

  [CLEAR_APP_ERRORS](state) {
    state.error = { show: false, values: [] };
  },

  [PUSH_APP_ERROR](state, err) {
    state.error = { show: true, values: [...state.error.values, err] };
  },
};
