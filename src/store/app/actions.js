import { SET_SHOW_FOOTER } from "@/store/mutation-types.js";
import { SHOW_FOOTER, HIDE_FOOTER } from "@/store/action-types.js";

export default {
  [SHOW_FOOTER]({ commit }) {
    commit(SET_SHOW_FOOTER, true);
  },
  [HIDE_FOOTER]({ commit }) {
    commit(SET_SHOW_FOOTER, false);
  },
};
