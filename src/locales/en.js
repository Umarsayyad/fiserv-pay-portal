export default {
  signIn: {
    login: "Login",
    username: "Username",
    password: "Password",
    signIn: "Sign In",
    forgotPassword: "Forgot Password?",
    usernameRequired: "Please enter your username.",
    passwordRequired: "Please enter your password.",
  },

  footer: {
    privacyPolicy: "Privacy policy",
    terms: "Terms of use",
    copyright: "Fiserv, Inc. or its affiliates",
    support: {
      terms: "Terms of use",
      privacyPolicy: "Privacy policy",
    },
    faqs: {
      label: "FAQs",
      acceptPayout: "Accepting a Payout",
      savingMethod: "Saving a Payout Method",
      cancelPayout: "Cancel a Payout",
      resolveDispute: "Resolve Payout Dispute",
    },
  },

  dashboard: {
    greeting: "Welcome",
    greetingSubtext: "Here are today's highlights",
    availablePayoutsList: {
      header: "Available Payout | Available Payouts",
      noPayouts: "No Available Payouts",
      expires: "Expires",
      reviewAll: "Review All",
      networkError: "There was an error retrieving your available payouts",
    },
    payoutsByMerchant: {
      networkError: "There was an error retrieving your payouts by merchant",
    },
    recentActivities: {
      title: "Recent Activity",
    },
    paymentMethods: {
      networkError: "There was an error retrieving your payment methods",
    },
  },

  dashboardTile: {
    availablePayouts: "Available Payouts",
    currentPayoutAmount: "Current Payout Amount",
    totalMoneyReceived: " Total Money Received",
    acceptAction: "Accept",
  },

  payoutByMerchant: {
    title: "Payout by Merchant",
    total: "Total",
  },

  navbar: {
    dashboard: "Dashboard",
    payouts: "Payouts",
    profileSettings: "Profile Settings",
    logout: "Logout",
  },

  paymentMethods: {
    title: "Payment Methods",
    addNew: "Add New",
    checking: "Checking",
    debit: "Debit",
    linkedApp: "Linked App",
    addPayoutMethod: "Add Payout Method",
  },
  payouts: {
    listView: {
      title: "Payouts",
      availableTab: "Available",
      historyTab: "History",
      history: {
        filterBy: "Filter By",
        filters: {
          all: "All",
          show: {
            label: "Show",
            last90Days: "Last 90 Days",
          },
          status: {
            label: "Status",
          },
          merchant: {
            label: "Merchant",
          },
        },
        pagination: {
          sizeLabel: "Items per page",
          totalLabel: "of {n} item | of {n} items",
        },
        networkError: "There was an error retrieving your payouts history",
      },
    },
  },

  paymentStatus: {
    completed: "Completed",
    accepted: "Accepted",
    rejected: "Rejected",
    canceled: "Canceled",
    expired: "Expired",
    reversalSuccess: "Reversal Success",
    reversalPending: "Reversal Pending",
    reversalFailed: "Reversal Failed",
  },

  profile: {
    title: "My Profile",
    edit: "Edit",
    profileInfo: {
      edit: "Edit Profile Info",
      name: "Name",
      DOBTIN: "Date of Birth/TIN",
      address: "Address",
      phoneNumber: "Phone Number",
      networkError: "There was an error retrieving your contact information",
      username: "Username",
    },
    emailSettings: {
      title: "Email Address",
      emailsOnFile: "Emails on File",
    },
    mySettings: {
      title: "My Settings",
    },
    alertPreferences: {
      title: "One-Time Passcode Delivery Preference",
      email: "Email",
      sms: "SMS",
    },
  },
  notFound: {
    pageNotFound: "Page not found",
    returnToDashboard: "Return to Dashboard",
  },
  newPayoutMethod: {
    readMore: "Read More",
    title: "Add New Payout Methods",
    info:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do\n" +
      "              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut\n" +
      "              enim ad minim venia.",
    bankAccount: {
      name: "Bank Account",
      timeFrame: "1-3 days",
      tooltip: `By choosing this method, your payment will be deposited 
      into your bank account within 1-3 business days. 
      Be sure to enter account information correctly to avoid any 
      issues with receiving your payment.`,
      addNew: {
        info:
          "To verify your bank account, there will be two transactions of $1 or less made during the next three days (one deposit and one withdrawal). To avoid any overdraft or other bank fees, make sure you have at least $2 in your bank account.",
        nameOfBank: "Name of Bank",
        accountType: {
          name: "Account Type",
          checking: "CHECKING",
          saving: "SAVING",
        },
        accountNumber: "Account Number",
        confirmAccountNumber: "Confirm Account Number",
        routingNumber: "Routing Number",
        routingNumberHelp: "What's My Routing Number?",
        routingNumberToolTip:
          "You can find your routing number at the bottom of a check.",
        routingOrTransitNumber: "Routing/Transit Number",
        checkNumber: "Check Number",
        viewAndAcceptTC: "I have viewed and accepted the ",
        termsAndConditions: "Terms and Conditions.",
        primaryPayoutMethod: "Make this my primary payout method.",
        addAccount: "Add Account",
        cancel: "Cancel",
      },
    },
    debitCard: {
      name: "Debit Card",
      timeFrame: "Instant",
      tooltip: `By choosing this method, you will receive your payment within minutes 
      to your Visa® or Mastercard® debit card. Be sure to enter card information 
      correctly to avoid any issues with receiving your payment. 
      Payments over $50,000 cannot be collected on the debit card.`,
    },
    venmo: {
      name: "Venmo",
      timeFrame: "Time",
      logo: "venmo",
      tooltip: `By selecting this option, your payment will be credited to 
      your existing Venmo account. Make sure the phone number you enter is correct
       to avoid any delays in processing your payment. Funds cannot be reversed 
       if you enter the wrong phone number.This payment should post to your Venmo account today.`,
    },
    paypal: {
      name: "Paypal",
      timeFrame: "Time",
      logo: "paypal",
      tooltip: `By selecting this option, your payment will be credited to
       your existing PayPal account. Make sure the email you enter is correct to avoid 
       any delays in processing your payment. 
       Funds cannot be reversed if you enter the wrong email.
        This payment should post to your PayPal account today.`,
    },
    moneyNetwork: {
      name: "Money Network",
      timeFrame: "Time",
      logo: "moneynetwork",
      tooltip: `Instant deposit to Money Network Card`,
    },
    errors: {
      confirmAccountNumbers:
        "Account number and confirm account number fields doesn't match.",
      requiredNumber: "This field allows only numbers.",
      requiredField: "This is required field.",
    },
  },
};
