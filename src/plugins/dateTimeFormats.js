export default {
  "en-US": {
    shortDate: {
      year: "numeric",
      month: "2-digit",
      day: "2-digit",
    },
    longDate: {
      year: "numeric",
      month: "long",
      day: "numeric",
    },
  },
};
