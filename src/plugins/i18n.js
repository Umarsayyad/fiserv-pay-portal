import Vue from "vue";
import VueI18n from "vue-i18n";
import messages from "@/locales/en";
import dateTimeFormats from "@/plugins/dateTimeFormats";

Vue.use(VueI18n);

export const i18n = new VueI18n({
  locale: "en-US",
  fallbackLocale: "en-US",
  messages: { "en-US": messages },
  dateTimeFormats,
});
