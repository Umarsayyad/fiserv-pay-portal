import Vue from "vue";
import Vuetify from "vuetify/lib/framework";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#000000",
        secondary: "#535353",
        accent: "#ff6700",
        success: "#4B8500",
        error: "#CC0000",
        background: "#f1f1f1",
        sidebar: "#e3e3e3",
        panel: "#f4f4f4",
        "sidebar-panel": "#d9d9d9",
      },
    },
  },
  icons: {
    iconfont: "mdiSvg",
  },
});
