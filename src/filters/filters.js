import { parse } from "date-fns";
import { i18n } from "@/plugins/i18n";

export function currency(value, currency = "USD") {
  if (typeof value !== "number") {
    throw "argument must be of type Number";
  }

  const formatter = new Intl.NumberFormat([i18n.locale, "en-US"], {
    style: "currency",
    currency: currency,
    minimumFractionDigits: 2,
  });

  return formatter.format(value);
}

export function longDate(value) {
  const parsed = parse(value, "MM/dd/yyyy", new Date());
  return i18n.d(parsed, "longDate");
}

export function shortDate(value) {
  const parsed = parse(value, "MM/dd/yyyy", new Date());
  return i18n.d(parsed, "shortDate");
}

export function capitalize(value) {
  if (!value) {
    return "";
  }

  const first = value.charAt(0).toUpperCase();
  const rest = value.slice(1).toLowerCase();
  return `${first}${rest}`;
}

export function maskAccountNumber(number) {
  const lastFour = number.slice(-4);
  const firstFour = "\u25CF" + "\u25CF" + "\u25CF" + "\u25CF";
  return `${firstFour} ${lastFour}`;
}

export function phoneNumber(number) {
  //gets rid of non-numeric characters
  const cleaned = ("" + number).replace(/\D/g, "");
  //looks for the pattern (+x | +xx | '') xxx-xxx-xxxx
  const match = cleaned.match(/^(\d|\d{2}|)?(\d{3})(\d{3})(\d{4})$/);
  if (match) {
    const intlCode = match[1];
    if (intlCode) {
      return ["+", intlCode, " ", match[2], "-", match[3], "-", match[4]].join(
        ""
      );
    } else {
      return [match[2], "-", match[3], "-", match[4]].join("");
    }
  }
  return number;
}
