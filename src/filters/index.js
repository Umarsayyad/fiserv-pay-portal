import Vue from "vue";
import {
  longDate,
  currency,
  capitalize,
  shortDate,
  maskAccountNumber,
  phoneNumber,
} from "@/filters/filters";

Vue.filter("currency", currency);
Vue.filter("longDate", longDate);
Vue.filter("shortDate", shortDate);
Vue.filter("capitalize", capitalize);
Vue.filter("maskAccountNumber", maskAccountNumber);
Vue.filter("phoneNumber", phoneNumber);
