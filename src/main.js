import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import amplify from "@/amplify";
import { Vuelidate } from "vuelidate";
import vuetify from "@/plugins/vuetify";
import { i18n } from "@/plugins/i18n";
import "@/filters";
import "@/scss/index.scss";

Vue.config.productionTip = false;

Vue.use(Vuelidate);

amplify.configure();

new Vue({
  router,
  store,
  vuetify,
  i18n,
  render: (h) => h(App),
}).$mount("#app");
