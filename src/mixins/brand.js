export default {
  computed: {
    brand() {
      if (!this.payout || !this.payout.recipient) {
        return null;
      }

      const recipient = this.payout.recipient[0];
      return {
        groupName: recipient.groupName,
        displayName: recipient.doingBusinessAs,
      };
    },
  },
};
