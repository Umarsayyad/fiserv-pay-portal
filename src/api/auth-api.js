import http from "@/api/clients/auth-client";

export default {
  createLimitedAccessToken(ak) {
    return http.post("/accessTokens", { customerAccessKey: ak });
  },
};
