import http from "@/api/clients/ddp-client";

export default {
  createOtp(email, operation) {
    return http.post("/otp", { email, operation });
  },
};
