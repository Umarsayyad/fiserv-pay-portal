import axios from "axios";

export default axios.create({
  baseURL: process.env.VUE_APP_AUTH_API_BASE_URL,
  headers: { "x-api-key": process.env.VUE_APP_AUTH_API_KEY },
});
