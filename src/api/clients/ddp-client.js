import axios from "axios";

export default axios.create({
  baseURL: process.env.VUE_APP_DDP_API_BASE_URL,
});
