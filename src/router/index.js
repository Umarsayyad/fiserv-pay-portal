import Vue from "vue";
import VueRouter from "vue-router";
import routes from "@/router/routes";
import { authenticate } from "@/router/guards";

Vue.use(VueRouter);

const router = new VueRouter({
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return { x: 0, y: 0 };
    }
  },
});

router.beforeEach(authenticate);

export default router;
