export default [
  // ROOT
  {
    path: "/",
    name: "dashboard",
    meta: { permitAll: true },
    component: () =>
      import(/* webpackChunkName: "dashboard" */ "../views/Dashboard.vue"),
  },

  {
    path: "/profile",
    name: "profile",
    meta: { permitAll: true },
    component: () =>
      import(/* webpackChunkName: "profile" */ "../views/Profile.vue"),
  },

  // AUTH
  {
    path: "/auth",
    meta: { permitAll: true },
    component: () =>
      import(/* webpackChunkName: "auth" */ "../views/auth/Auth.vue"),
    children: [
      {
        path: "",
        redirect: { name: "sign-in" },
      },
      {
        path: "sign-in",
        name: "sign-in",
        component: () =>
          import(/* webpackChunkName: "sign-in" */ "../views/auth/SignIn.vue"),
      },
      {
        path: "forgot-password",
        name: "forgot-password",
        component: () =>
          import(
            /* webpackChunkName: "forgot-password" */ "../views/auth/ForgotPassword.vue"
          ),
      },
      {
        path: "complete-new-password",
        name: "complete-new-password",
        component: () =>
          import(
            /* webpackChunkName: "complete-new-password" */ "../views/auth/CompleteNewPassword.vue"
          ),
      },
      {
        path: "forgot-password-submit",
        name: "forgot-password-submit",
        component: () =>
          import(
            /* webpackChunkName: "forgot-password-submit" */ "../views/auth/ForgotPasswordSubmit.vue"
          ),
        props: (route) => ({ username: route.query.username }),
      },
      {
        path: "confirm-sign-in",
        name: "confirm-sign-in",
        component: () =>
          import(
            /* webpackChunkName: "confirm-sign-in" */ "../views/auth/ConfirmSignIn.vue"
          ),
      },
    ],
  },

  // LEGAL
  {
    path: "/legal",
    meta: { permitAll: true },
    component: () =>
      import(/* webpackChunkName: "legal" */ "../views/legal/Legal.vue"),
    children: [
      {
        path: "",
        redirect: { name: "privacy-policy" },
      },
      {
        path: "privacy-policy",
        name: "privacy-policy",
        component: () =>
          import(
            /* webpackChunkName: "privacy-policy" */ "../views/legal/PrivacyPolicy.vue"
          ),
      },
      {
        path: "terms",
        name: "terms",
        component: () =>
          import(
            /* webpackChunkName: "terms" */ "../views/legal/TermsAndConditions.vue"
          ),
      },
    ],
  },

  // PAYOUTS
  {
    path: "/payouts",
    name: "payouts",
    meta: { permitAll: true },
    component: () =>
      import(/* webpackChunkName: "payouts" */ "../views/Payouts.vue"),
    props: (route) => ({ initActive: route.query.active }),
  },

  // 404
  {
    path: "*",
    name: "not-found",
    meta: { permitAll: true },
    component: () =>
      import(/* webpackChunkName: "not-found" */ "../views/NotFound.vue"),
  },
];
