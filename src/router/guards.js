import store from "@/store";
import { CURRENT_AUTHENTICATED_USER } from "@/store/action-types";
import { SET_REDIRECT } from "@/store/mutation-types";

export const authenticate = async (to, _, next) => {
  if (to.matched.some((record) => record.meta.permitAll)) {
    return next();
  }

  try {
    await store.dispatch(CURRENT_AUTHENTICATED_USER);
    return next();
  } catch (e) {
    store.commit(SET_REDIRECT, to);
    return next({ name: "sign-in" });
  }
};
