import {
  longDate,
  currency,
  capitalize,
  shortDate,
  maskAccountNumber,
  phoneNumber,
} from "@/filters/filters";

describe("filters", () => {
  it("returns formatted US currency", () => {
    const cases = [
      { value: 200, currency: "USD", expected: "$200.00" },
      { value: 200.0, currency: "USD", expected: "$200.00" },
      { value: 0, currency: "USD", expected: "$0.00" },
      { value: 201, currency: "EUR", expected: "€201.00" },
    ];

    cases.forEach((tc) => {
      const actual = currency(tc.value, tc.currency);
      expect(actual).toBe(tc.expected);
    });
  });

  it("throws an error when trying to format a non-numeric value", () => {
    const t = () => currency("bad input");
    expect(t).toThrow("argument must be of type Number");
  });

  it("converts a short date to a long date", () => {
    const actual = longDate("01/01/1981");
    expect(actual).toBe("January 1, 1981");
  });

  it("localizes a short date", () => {
    const actual = shortDate("01/01/1981");
    expect(actual).toBe("01/01/1981");
  });

  it("capitalizes a string", () => {
    const cases = [
      { value: "TYRION", expected: "Tyrion" },
      { value: "tyrion", expected: "Tyrion" },
      { value: "t", expected: "T" },
      { value: "", expected: "" },
      { value: null, expected: "" },
    ];
    cases.forEach((tc) => {
      const actual = capitalize(tc.value);
      expect(actual).toBe(tc.expected);
    });
  });

  it("appends 4 black password dots before the last 4 digits of a number", () => {
    const number = "123456789";
    const maskedNumber = maskAccountNumber(number);
    expect(maskedNumber).toBe(
      "\u25CF" + "\u25CF" + "\u25CF" + "\u25CF" + " " + "6789"
    );
  });

  it("formats domestic phone numbers to xxx-xxx-xxxx and international to (+x | +xx) xxx-xxx-xxxx ", () => {
    const number = "1234567890";
    const intlNumber = "+1 1234567890";
    const longIntlNumber = "+47 1234567890";
    const formatted = phoneNumber(number);
    const intlFormatted = phoneNumber(intlNumber);
    const longIntlNumberFormatted = phoneNumber(longIntlNumber);

    expect(formatted).toBe("123-456-7890");
    expect(intlFormatted).toBe("+1 123-456-7890");
    expect(longIntlNumberFormatted).toBe("+47 123-456-7890");
  });

  it("returns an error message when phone number isn't formatted correctly ", () => {
    const invalidNumber = "1234";
    const formattedInvalidNumber = phoneNumber(invalidNumber);
    expect(formattedInvalidNumber).toBe("1234");
  });
});
