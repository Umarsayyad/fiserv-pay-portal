import { SET_REDIRECT } from "@/store/mutation-types";

const { authenticate } = require("@/router/guards");
import store from "@/store";

jest.mock("@/store");

describe("router/guards", () => {
  it("skips auth on permitAll routes", () => {
    store.dispatch.mockResolvedValue(undefined);

    const to = { matched: [{ meta: { permitAll: true } }] };
    const next = jest.fn();

    authenticate(to, null, next).then(() => {
      expect(next).toHaveBeenCalled();
      expect(store.dispatch).not.toHaveBeenCalled();
    });
  });

  it("authenticates the route successfully", () => {
    store.dispatch.mockResolvedValue({ username: "tyrion.lannister" });

    const to = { matched: [{ meta: {} }] };
    const next = jest.fn();

    authenticate(to, null, next).then(() => {
      expect(store.dispatch).toHaveBeenCalled();
      expect(next).toHaveBeenCalled();
    });
  });

  it("redirects to auth when authentication fails", () => {
    store.dispatch.mockRejectedValue(undefined);

    const to = { matched: [{ meta: {} }] };
    const next = jest.fn();

    authenticate(to, null, next).then(() => {
      expect(store.dispatch).toHaveBeenCalled();
      expect(store.commit).toHaveBeenCalledWith(SET_REDIRECT, to);
      expect(next).toBeCalledWith({
        name: "sign-in",
      });
    });
  });
});
