import brand from "@/mixins/brand";
import { shallowMount } from "@vue/test-utils";

describe("mixins/brand", () => {
  it("computes the brand", async () => {
    const Component = {
      template: "<div>component</div>",
      props: ["payout"],
      mixins: [brand],
    };

    const wrapper = shallowMount(Component);
    expect(wrapper.vm.brand).toBeNull();

    await wrapper.setProps({ payout });
    expect(wrapper.vm.brand).toEqual({
      groupName: "statefarm",
      displayName: "State Farm",
    });
  });
});

const payout = {
  transactionId: "TID-ee1f4255-3c2e-45b6-8028-5c2b587fde4c",
  created: 1618592828,
  merchantTransactionId: "TransactionID-1991310",
  paymentReferenceNumber: "1234567904",
  applyDate: "04/16/2021",
  transactionDate: "04/16/2021",
  updateDate: "04/16/2021",
  recipient: [
    {
      merchantCustomerId: "merchantCustomerId-29764558",
      recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
      payments: {
        paymentId: "47884808-337f-4a19-83d1-5cb6a16189a7",
        amount: {
          total: 201,
          currency: "USD",
        },
        fee: {
          total: 0,
          currency: "USD",
        },
        paymentType: "Claims",
        paymentStatus: "DP",
      },
      doingBusinessAs: "State Farm",
      firstName: "TYRION",
      lastName: "LANNISTER",
      groupName: "statefarm",
    },
  ],
  transactionStatus: "PD",
  totalTransactionAmount: {
    total: 201,
    currency: "USD",
  },
};
