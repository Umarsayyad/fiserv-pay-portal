import App from "@/App";
import TheFooter from "@/components/TheFooter";
import Vuex from "vuex";
import { createWrapper } from "../test-wrapper";
import app from "@/store/app/state";
import auth from "@/store/auth/state";

let state;

describe("App.vue", () => {
  beforeEach(() => {
    state = { app: app(), auth: auth() };
  });

  function newWrapper(store) {
    return createWrapper(App, {
      store,
      stubs: ["router-view", "router-link", "v-tab"],
      mocks: {
        $route: {
          name: "dashboard",
        },
      },
    });
  }

  it("renders the footer", () => {
    const getters = { isAuthenticated: () => false };
    const store = new Vuex.Store({ state, getters });
    const wrapper = newWrapper(store);
    const footer = wrapper.findComponent(TheFooter);
    expect(footer.exists()).toBeTruthy();
  });

  it("hides the footer", () => {
    state.app.showFooter = false;
    const getters = { isAuthenticated: () => false };
    const store = new Vuex.Store({ state, getters });
    const wrapper = newWrapper(store);
    const footer = wrapper.findComponent(TheFooter);
    expect(footer.exists()).toBeFalsy();
  });
});
