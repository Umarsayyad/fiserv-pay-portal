import Amplify from "aws-amplify";
import amplify from "@/amplify";

jest.mock("aws-amplify");

describe("amplify configuration", () => {
  it("configures amplify", () => {
    Amplify.configure.mockReturnValue(undefined);

    amplify.configure();

    expect(Amplify.configure).toHaveBeenCalled();
  });
});
