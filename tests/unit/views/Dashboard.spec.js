import { createWrapper } from "../../test-wrapper";
import Dashboard from "@/views/Dashboard";
import { PUSH_APP_ERROR, SET_SPLIT_BACKGROUND } from "@/store/mutation-types";
import {
  FETCH_AVAILABLE_PAYOUTS,
  FETCH_PAYMENT_METHODS,
  FETCH_TOTAL_PAYOUT_AMOUNTS_BY_MERCHANT,
} from "@/store/action-types";
import Vuex from "vuex";
import { mockTotalPayoutAmountsByMerchant } from "@/store/payouts/mock-data";
import PayoutByMerchant from "@/components/payouts-by-merchant/PayoutByMerchant";
import merchantStore from "@/store/merchant";
import paymentMethodsStore from "@/store/payment-methods";
import authStore from "@/store/auth";

const store = new Vuex.Store({
  modules: {
    merchant: merchantStore,
    paymentMethods: paymentMethodsStore,
    auth: authStore,
  },
});

store.dispatch = jest.fn();
store.commit = jest.fn();

describe("Dashboard.vue", () => {
  beforeEach(() => {
    store.dispatch.mockReset();
    store.commit.mockReset();
  });

  it("commits splitBackground true on mounted", () => {
    store.dispatch.mockResolvedValue([]);
    createWrapper(Dashboard, { store });

    expect(store.commit).toHaveBeenCalledWith(SET_SPLIT_BACKGROUND, true);
  });

  it("commits splitBackground false on destroyed", () => {
    store.dispatch.mockResolvedValue([]);
    const wrapper = createWrapper(Dashboard, { store });

    wrapper.destroy();

    expect(store.commit).toHaveBeenCalledWith(SET_SPLIT_BACKGROUND, false);
  });

  it("fetches required data", () => {
    store.dispatch.mockResolvedValue([]);
    createWrapper(Dashboard, { store });

    expect(store.dispatch).toHaveBeenCalledWith(FETCH_AVAILABLE_PAYOUTS);
    expect(store.dispatch).toHaveBeenCalledWith(
      FETCH_TOTAL_PAYOUT_AMOUNTS_BY_MERCHANT
    );
    expect(store.dispatch).toHaveBeenCalledWith(FETCH_PAYMENT_METHODS);
  });

  it("displays an error when fetch available payouts fails", async () => {
    store.dispatch.mockRejectedValueOnce({
      msg: "something bad happened",
    });
    store.dispatch.mockResolvedValue([]);

    const wrapper = createWrapper(Dashboard, { store });
    await wrapper.vm.$nextTick();

    expect(store.commit).toHaveBeenCalledWith(
      PUSH_APP_ERROR,
      "There was an error retrieving your available payouts"
    );
  });

  it("displays an error when fetch payout totals by merchant fails", async () => {
    store.dispatch.mockResolvedValueOnce({
      msg: "ok",
    });

    store.dispatch.mockRejectedValueOnce({
      msg: "something bad happened",
    });

    store.dispatch.mockResolvedValueOnce([]);

    const wrapper = createWrapper(Dashboard, { store });
    await wrapper.vm.$nextTick();

    expect(store.commit).toHaveBeenCalledWith(
      PUSH_APP_ERROR,
      "There was an error retrieving your payouts by merchant"
    );
  });

  it("displays an error when fetch payment methods fails", async () => {
    store.dispatch.mockResolvedValueOnce([]);
    store.dispatch.mockResolvedValueOnce([]);
    store.dispatch.mockRejectedValueOnce({
      msg: "something bad happened",
    });

    const wrapper = createWrapper(Dashboard, { store });
    await wrapper.vm.$nextTick();

    expect(store.commit).toHaveBeenCalledWith(
      PUSH_APP_ERROR,
      "There was an error retrieving your payment methods"
    );
  });

  it("renders payouts by merchant", async () => {
    store.dispatch.mockResolvedValueOnce([]);
    store.dispatch.mockResolvedValueOnce(mockTotalPayoutAmountsByMerchant);
    store.dispatch.mockResolvedValue([]);

    const wrapper = createWrapper(Dashboard, { store });
    await wrapper.vm.$nextTick();

    const pbm = wrapper.findComponent(PayoutByMerchant);
    expect(pbm.exists()).toBeTruthy();
  });

  it("doesn't render payouts by merchant when there are no payouts by merchant", async () => {
    store.dispatch.mockResolvedValue([]);

    const wrapper = createWrapper(Dashboard, { store });
    await wrapper.vm.$nextTick();

    const pbm = wrapper.findComponent(PayoutByMerchant);
    expect(pbm.exists()).toBeFalsy();
  });

  it("verify dashboard tiles", () => {
    store.dispatch.mockResolvedValue([]);
    const wrapper = createWrapper(Dashboard, { store });

    const availablePayouts = wrapper.find(
      "div[data-test=available-payouts-tile]"
    );
    expect(availablePayouts.exists()).toBeTruthy();

    const currentPayouts = wrapper.find("div[data-test=current-payouts-tile]");
    expect(currentPayouts.exists()).toBeTruthy();

    const totalMoneyReceived = wrapper.find(
      "div[data-test=total-money-received-tile]"
    );
    expect(totalMoneyReceived.exists()).toBeTruthy();
  });
});
