import ForgotPassword from "@/views/auth/ForgotPassword.vue";
import { FORGOT_PASSWORD } from "@/store/action-types";
import router from "@/router";
import store from "@/store";
import { createWrapper } from "../../../test-wrapper";

jest.mock("@/store");
jest.mock("@/router");

function newWrapper() {
  const options = {
    mocks: {
      $store: store,
      $router: router,
    },
  };

  return createWrapper(ForgotPassword, options);
}

describe("ForgotPassword.vue", () => {
  beforeEach(() => {
    store.dispatch.mockReset();
    router.push.mockReset();
  });

  it("navigates to forgotPasswordSubmit.vue when button is clicked", async () => {
    const username = "tyrion.lannister";

    const wrapper = newWrapper();
    await wrapper.setData({ username });
    store.dispatch.mockResolvedValue(undefined);

    await wrapper.find("button[data-test=submit-btn]").trigger("click");

    expect(store.dispatch).toHaveBeenCalledWith(FORGOT_PASSWORD, username);
    expect(router.push).toHaveBeenCalledWith({
      name: "forgot-password-submit",
      query: { username: username },
    });
  });

  it("navigates to forgotPasswordSubmit.vue when the enter key is pressed on the username field", async () => {
    const username = "tyrion.lannister";

    const wrapper = newWrapper();
    await wrapper.setData({ username });
    store.dispatch.mockResolvedValue(undefined);
    const input = wrapper.find("input[data-test=username-input]");
    await input.trigger("keyup.enter");

    expect(store.dispatch).toHaveBeenCalledWith(FORGOT_PASSWORD, username);
    expect(router.push).toHaveBeenCalledWith({
      name: "forgot-password-submit",
      query: { username: username },
    });
  });

  it("doesn't attempt to navigate if the form is invalid", async () => {
    const wrapper = newWrapper();

    const input = wrapper.find("input[data-test=username-input]");
    await input.trigger("keyup.enter");

    expect(store.dispatch).not.toHaveBeenCalled();
  });

  it("disables the submit button when pristine", () => {
    const wrapper = newWrapper();

    const submitButton = wrapper.find("button[data-test=submit-btn]");
    expect(submitButton.element.disabled).toBeTruthy();
  });

  it("enables the submit button when the form is valid", async () => {
    const wrapper = newWrapper();
    await wrapper.setData({ username: "tyrion.lannister" });

    const submitButton = wrapper.find("button[data-test=submit-btn]");
    expect(submitButton.element.disabled).toBeFalsy();
  });

  it("focuses the username input by default", () => {
    const wrapper = newWrapper(true);

    const input = wrapper.find("input[data-test=username-input]").element;

    expect(document.activeElement).toBe(input);
  });

  it("displays username errors", async () => {
    const wrapper = newWrapper();

    // make it dirty
    const input = wrapper.find("input[data-test=username-input]");
    await input.trigger("blur");

    // empty
    let errs = wrapper.find("div.v-messages__message");
    expect(errs.text()).toBe("Please enter your username.");

    // happy
    await wrapper.setData({ username: "t" });
    errs = wrapper.find("div.v-messages__message");
    expect(errs.exists()).toBeFalsy();
  });

  it("disables the button while redirecting", async () => {
    const wrapper = newWrapper();
    await wrapper.setData({
      loading: true,
      username: "tyrion.lannister",
    });

    const button = wrapper.find('button[data-test="submit-btn"]');
    expect(button.element.disabled).toBeTruthy();
  });

  it("displays any network errors", async () => {
    const errMsg = "Username/client id combination not found.";
    store.dispatch.mockRejectedValue({ message: errMsg });

    const wrapper = newWrapper();
    await wrapper.setData({ username: "3274238" });

    await wrapper.find("button[data-test=submit-btn]").trigger("click");
    await wrapper.vm.$nextTick();

    const error = wrapper.find("span[data-test=network-error]");
    expect(error.text()).toBe(errMsg);
    expect(router.push).not.toHaveBeenCalled();
  });
});
