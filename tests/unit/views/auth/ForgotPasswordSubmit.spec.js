import ForgotPasswordSubmit from "@/views/auth/ForgotPasswordSubmit.vue";
import { FORGOT_PASSWORD_SUBMIT } from "@/store/action-types";
import router from "@/router";
import store from "@/store";
import { createWrapper } from "../../../test-wrapper";

jest.mock("@/store");
jest.mock("@/router");

const username = "tyrion.lannister";

function newWrapper() {
  const options = {
    mocks: {
      $store: store,
      $router: router,
    },
    propsData: { username },
  };

  return createWrapper(ForgotPasswordSubmit, options);
}

describe("ForgotPasswordSubmit.vue", () => {
  beforeEach(() => {
    store.dispatch.mockReset();
    router.replace.mockReset();
  });

  it("submits new password and returns to homepage when submit button is clicked", async () => {
    const wrapper = newWrapper();
    const form = { otp: "123456", newPassword: "fiserv123" };

    await wrapper.setData({ form });
    store.dispatch.mockResolvedValue(undefined);

    await wrapper.find("button[data-test=submit-btn]").trigger("click");

    expect(store.dispatch).toHaveBeenCalledWith(FORGOT_PASSWORD_SUBMIT, {
      ...form,
      username,
    });
    expect(router.replace).toHaveBeenCalledWith({ name: "sign-in" });
  });

  it("doesn't attempt to sign in if the form is invalid", async () => {
    const wrapper = newWrapper();

    const input = wrapper.find("input[data-test=new-password-input]");
    await input.trigger("keyup.enter");

    expect(store.dispatch).not.toHaveBeenCalled();
  });

  it("disables the submit button when pristine", () => {
    const wrapper = newWrapper();

    const submitButton = wrapper.find("button[data-test=submit-btn]");
    expect(submitButton.element.disabled).toBeTruthy();
  });

  it("focuses the otp input by default", () => {
    const wrapper = newWrapper(true);

    const input = wrapper.find("input[data-test=otp-input]").element;

    expect(document.activeElement).toBe(input);
  });

  it("displays otp errors", async () => {
    const wrapper = newWrapper();
    await wrapper.setData({
      form: { otp: null, newPassword: "password123" },
    });
    // make it dirty
    const input = wrapper.find("input[data-test=otp-input]");
    await input.trigger("blur");

    // empty
    let errs = wrapper.find("div.v-messages__message");
    expect(errs.text()).toBe("Recovery passcode is required.");

    // happy
    await wrapper.setData({ form: { otp: "123123" } });
    expect(wrapper.vm.otpErrors).toEqual([]);

    errs = wrapper.find("div.v-messages__message");
    expect(errs.exists()).toBeFalsy();
  });

  it("disables the button while redirecting", async () => {
    const wrapper = newWrapper();

    await wrapper.setData({
      loading: true,
      username: "tyrion.lannister",
    });

    const btn = wrapper.find("button[data-test=submit-btn]");
    expect(btn.element.disabled).toBeTruthy();
  });

  it("displays new password errors", async () => {
    const wrapper = newWrapper();

    // make it dirty
    const input = wrapper.find("input[data-test=new-password-input]");
    await input.trigger("blur");

    // empty
    let errs = wrapper.find("div.v-messages__message");
    expect(errs.text()).toBe("A new password is required.");

    // happy
    await wrapper.setData({ form: { newPassword: "password" } });
    errs = wrapper.find("div.v-messages__message");
    expect(errs.exists()).toBeFalsy();

    await wrapper.setData({ form: { newPassword: "pass" } });
    errs = wrapper.find("div.v-messages__message");
    expect(errs.text()).toBe("Password should be at least 8 characters.");
  });

  it("displays any network errors", async () => {
    const errMsg = "Invalid verification code provided, please try again.";
    store.dispatch.mockRejectedValue({ message: errMsg });

    const wrapper = newWrapper();
    await wrapper.setData({
      form: { otp: "2342332222", newPassword: "fiserv123" },
    });

    await wrapper.find("button[data-test=submit-btn]").trigger("click");
    await wrapper.vm.$nextTick();

    const error = wrapper.find("span[data-test=network-error]");
    expect(error.text()).toBe(errMsg);
    expect(router.push).not.toHaveBeenCalled();
  });
});
