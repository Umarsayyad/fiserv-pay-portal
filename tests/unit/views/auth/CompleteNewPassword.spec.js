import router from "@/router";
import { COMPLETE_NEW_PASSWORD } from "@/store/action-types";
import CompleteNewPassword from "@/views/auth/CompleteNewPassword";
import { createWrapper } from "../../../test-wrapper";

const store = {
  dispatch: jest.fn(),
  state: { auth: { redirect: { name: "home" } } },
};
jest.mock("@/router");

function newWrapper() {
  const options = {
    mocks: {
      $store: store,
      $router: router,
    },
  };

  return createWrapper(CompleteNewPassword, options);
}

describe("CompleteNewPassword.vue", () => {
  beforeEach(() => {
    store.dispatch.mockReset();
    router.push.mockReset();
    router.replace.mockReset();
  });

  it("disables the change button when pristine", () => {
    const wrapper = newWrapper();

    const changeBtn = wrapper.find("button[data-test=change-btn]");
    expect(changeBtn.element.disabled).toBeTruthy();
  });

  it("enables the change button when the form is valid", async () => {
    const wrapper = newWrapper();
    await wrapper.setData({ newPassword: "password" });

    const changeButton = wrapper.find("button[data-test=change-btn]");
    expect(changeButton.element.disabled).toBeFalsy();
  });

  it("disables the change button while working", async () => {
    const wrapper = newWrapper();
    await wrapper.setData({ loading: true, newPassword: "password" });

    const changeButton = wrapper.find("button[data-test=change-btn]");
    expect(changeButton.element.disabled).toBeTruthy();
  });

  it("navigates to the home screen after changing", async () => {
    const newPassword = "password";

    const wrapper = newWrapper();
    await wrapper.setData({ newPassword });

    store.dispatch.mockResolvedValue({ challengeName: "" });
    await wrapper.find("button[data-test=change-btn]").trigger("click");

    expect(store.dispatch).toHaveBeenCalledWith(
      COMPLETE_NEW_PASSWORD,
      newPassword
    );
    expect(router.replace).toHaveBeenCalledWith(store.state.auth.redirect);
  });

  it("navigates to the confirm sign in screen if mfa enabled", async () => {
    const newPassword = "password";

    const wrapper = newWrapper();
    await wrapper.setData({ newPassword });

    store.dispatch.mockResolvedValue({ challengeName: "SMS_MFA" });
    await wrapper.find("button[data-test=change-btn]").trigger("click");

    expect(store.dispatch).toHaveBeenCalledWith(
      COMPLETE_NEW_PASSWORD,
      newPassword
    );
    expect(router.replace).toHaveBeenCalledWith({ name: "confirm-sign-in" });
  });

  it("changes the password when the enter key is pressed and the form is valid", async () => {
    store.dispatch.mockResolvedValue({ challengeName: "" });

    const newPassword = "password";

    const wrapper = newWrapper();
    await wrapper.setData({ newPassword });

    const input = wrapper.find("input[data-test=new-password-input]");
    await input.trigger("keyup.enter");

    expect(store.dispatch).toHaveBeenCalledWith(
      COMPLETE_NEW_PASSWORD,
      newPassword
    );
    expect(router.replace).toHaveBeenCalledWith({ name: "home" });
  });

  it("doesn't attempt to change the password if the form is invalid", async () => {
    const wrapper = newWrapper();

    const input = wrapper.find("input[data-test=new-password-input]");
    await input.trigger("keyup.enter");

    expect(store.dispatch).not.toHaveBeenCalled();
  });

  it("focuses the input by default", () => {
    const wrapper = newWrapper(true);
    const input = wrapper.find("input[data-test=new-password-input]").element;
    expect(document.activeElement).toBe(input);
  });

  it("displays any network errors", async () => {
    const errMsg = "Something went wrong";
    store.dispatch.mockRejectedValue({ message: errMsg });

    const wrapper = newWrapper();
    await wrapper.setData({ newPassword: "password" });

    await wrapper.find("button[data-test=change-btn]").trigger("click");
    await wrapper.vm.$nextTick();

    const error = wrapper.find("span[data-test=network-error]");
    expect(error.text()).toBe(errMsg);
    expect(router.push).not.toHaveBeenCalled();
  });

  it("displays password errors", async () => {
    const wrapper = newWrapper();

    // make it dirty
    const input = wrapper.find("input[data-test=new-password-input]");
    await input.trigger("blur");

    // empty
    let errs = wrapper.find("div.v-messages__message");
    expect(errs.text()).toBe("Please enter a new password.");

    // short
    await wrapper.setData({ newPassword: "passwor" });
    errs = wrapper.find("div.v-messages__message");
    expect(errs.text()).toBe(
      `Password needs to be at least ${wrapper.vm.$v.newPassword.$params.minLength.min} characters.`
    );

    // happy
    await wrapper.setData({ newPassword: "password" });
    errs = wrapper.find("div.v-messages__message");
    expect(errs.exists()).toBeFalsy();
  });
});
