import SignIn from "@/views/auth/SignIn";
import router from "@/router";
import { SIGN_IN } from "@/store/action-types";
import { createWrapper } from "../../../test-wrapper";

const store = {
  dispatch: jest.fn(),
  state: { auth: { redirect: { name: "home" } } },
};
jest.mock("@/router");

function newWrapper() {
  const options = {
    mocks: {
      $store: store,
      $router: router,
    },
  };

  return createWrapper(SignIn, options);
}

describe("SignIn.vue", () => {
  beforeEach(() => {
    store.dispatch.mockReset();
    router.push.mockReset();
    router.replace.mockReset();
  });

  it("disables the sign in button when pristine", () => {
    const wrapper = newWrapper();

    const signInButton = wrapper.find('button[data-test="sign-in-btn"]');
    expect(signInButton.element.disabled).toBeTruthy();
  });

  it("enables the button when the form is valid", async () => {
    const wrapper = newWrapper();
    await wrapper.setData({
      credentials: { username: "tyrion.lannister", password: "password" },
    });

    const signInButton = wrapper.find('button[data-test="sign-in-btn"]');
    expect(signInButton.element.disabled).toBeFalsy();
  });

  it("disables the button while signing in", async () => {
    const wrapper = newWrapper();
    await wrapper.setData({
      loading: true,
      credentials: { username: "tyrion.lannister", password: "password" },
    });

    const signInButton = wrapper.find('button[data-test="sign-in-btn"]');
    expect(signInButton.element.disabled).toBeTruthy();
  });

  it("navigates to the home screen after signing in", async () => {
    const credentials = {
      username: "tyrion.lannister",
      password: "password",
    };

    const wrapper = newWrapper();
    await wrapper.setData({ credentials });

    store.dispatch.mockResolvedValue({ challengeName: "" });

    await wrapper.find('button[data-test="sign-in-btn"]').trigger("click");

    expect(store.dispatch).toHaveBeenCalledWith(SIGN_IN, credentials);
    expect(router.push).toHaveBeenCalledWith({ name: "home" });
  });

  it("navigates to the confirm sign in screen if mfa enabled", async () => {
    const credentials = {
      username: "tyrion.lannister",
      password: "password",
    };

    const wrapper = newWrapper();
    await wrapper.setData({ credentials });

    store.dispatch.mockResolvedValue({ challengeName: "SMS_MFA" });
    await wrapper.find('button[data-test="sign-in-btn"]').trigger("click");

    expect(store.dispatch).toHaveBeenCalledWith(SIGN_IN, credentials);
    expect(router.push).toHaveBeenCalledWith({ name: "confirm-sign-in" });
  });

  it("navigates to the complete new password screen when required", async () => {
    const credentials = { username: "tyrion.lannister", password: "password" };

    const wrapper = newWrapper();
    await wrapper.setData({ credentials });

    store.dispatch.mockResolvedValue({
      challengeName: "NEW_PASSWORD_REQUIRED",
    });
    await wrapper.find('button[data-test="sign-in-btn"]').trigger("click");

    expect(store.dispatch).toHaveBeenCalledWith(SIGN_IN, credentials);
    expect(router.push).toHaveBeenCalledWith({ name: "complete-new-password" });
  });

  it("signs in when the enter key is pressed on the password field", async () => {
    store.dispatch.mockResolvedValue({ challengeName: "" });

    const credentials = { username: "tyrion.lannister", password: "password" };

    const wrapper = newWrapper();
    await wrapper.setData({ credentials });

    const input = wrapper.find("input[data-test=password-input]");
    await input.trigger("keyup.enter");

    expect(store.dispatch).toHaveBeenCalledWith(SIGN_IN, credentials);
    expect(router.push).toHaveBeenCalledWith({ name: "home" });
  });

  it("doesn't attempt to sign in if the form is invalid", async () => {
    const wrapper = newWrapper();

    const input = wrapper.find("input[data-test=password-input]");
    await input.trigger("keyup.enter");

    expect(store.dispatch).not.toHaveBeenCalled();
  });

  it("focuses the username input by default", () => {
    const wrapper = newWrapper(true);

    const input = wrapper.find("input[data-test=username-input]").element;

    expect(document.activeElement).toBe(input);
  });

  it("displays any network errors", async () => {
    const errMsg = "Username or password incorrect";
    store.dispatch.mockRejectedValue({ message: errMsg });

    const wrapper = newWrapper();
    await wrapper.setData({
      credentials: { username: "tyrion.lannister", password: "badpassword" },
    });

    await wrapper.find('button[data-test="sign-in-btn"]').trigger("click");
    await wrapper.vm.$nextTick();

    const error = wrapper.find('span[data-test="network-error"]');
    expect(error.text()).toBe(errMsg);
    expect(router.push).not.toHaveBeenCalled();
  });

  it("displays username errors", async () => {
    const wrapper = newWrapper();

    // make it dirty
    const input = wrapper.find("input[data-test=username-input]");
    await input.trigger("blur");

    // empty
    let errs = wrapper.find("div.v-messages__message");
    expect(errs.text()).toBe("Please enter your username.");

    // happy
    await wrapper.setData({ credentials: { username: "t" } });
    errs = wrapper.find("div.v-messages__message");
    expect(errs.exists()).toBeFalsy();
  });

  it("displays password errors", async () => {
    const wrapper = newWrapper();

    // make it dirty
    const input = wrapper.find("input[data-test=password-input]");
    await input.trigger("blur");

    // empty
    let errs = wrapper.find("div.v-messages__message");
    expect(errs.text()).toBe("Please enter your password.");

    // happy
    await wrapper.setData({ credentials: { password: "t" } });
    errs = wrapper.find("div.v-messages__message");
    expect(errs.exists()).toBeFalsy();
  });
});
