import router from "@/router";
import { CONFIRM_SIGN_IN } from "@/store/action-types";
import ConfirmSignIn from "@/views/auth/ConfirmSignIn";
import { createWrapper } from "../../../test-wrapper";

const store = {
  dispatch: jest.fn(),
  state: { auth: { redirect: { name: "home" } } },
};
jest.mock("@/router");

function newWrapper() {
  const options = {
    mocks: {
      $store: store,
      $router: router,
    },
  };

  return createWrapper(ConfirmSignIn, options);
}

describe("ConfirmSignIn.vue", () => {
  beforeEach(() => {
    store.dispatch.mockReset();
    router.replace.mockReset();
  });

  it("disables the confirm button when pristine", () => {
    const wrapper = newWrapper();

    const changeBtn = wrapper.find("button[data-test=confirm-btn");
    expect(changeBtn.element.disabled).toBeTruthy();
  });

  it("enables the confirm button when the form is valid", async () => {
    const wrapper = newWrapper();
    await wrapper.setData({ code: "123456" });

    const changeButton = wrapper.find("button[data-test=confirm-btn");
    expect(changeButton.element.disabled).toBeFalsy();
  });

  it("disables the confirm button while working", async () => {
    const wrapper = newWrapper();
    await wrapper.setData({ loading: true, code: "123456" });

    const changeButton = wrapper.find("button[data-test=confirm-btn");
    expect(changeButton.element.disabled).toBeTruthy();
  });

  it("navigates to the home screen after confirming", async () => {
    const code = "123456";

    const wrapper = newWrapper();
    await wrapper.setData({ code: code });

    store.dispatch.mockResolvedValue({ challengeName: "" });
    await wrapper.find("button[data-test=confirm-btn").trigger("click");

    expect(store.dispatch).toHaveBeenCalledWith(CONFIRM_SIGN_IN, code);
    expect(router.replace).toHaveBeenCalledWith({ name: "home" });
  });

  it("confirms the sign in when the enter key is pressed and the form is valid", async () => {
    store.dispatch.mockResolvedValue({ challengeName: "" });

    const code = "123456";

    const wrapper = newWrapper();
    await wrapper.setData({ code: code });

    const input = wrapper.find("input[data-test=code-input");
    await input.trigger("keyup.enter");

    expect(store.dispatch).toHaveBeenCalledWith(CONFIRM_SIGN_IN, code);
    expect(router.replace).toHaveBeenCalledWith({ name: "home" });
  });

  it("doesn't attempt to confirm sign in if the form is invalid", async () => {
    const wrapper = newWrapper();

    const input = wrapper.find("input[data-test=code-input]");
    await input.trigger("keyup.enter");

    expect(store.dispatch).not.toHaveBeenCalled();
  });

  it("focuses the code input by default", () => {
    const wrapper = newWrapper(true);
    const input = wrapper.find("input[data-test=code-input]").element;
    expect(document.activeElement).toBe(input);
  });

  it("displays wrapped network errors", async () => {
    const wrappedErr = {
      code: "SomeBadException",
      message: "Wrapped error returned",
    };
    store.dispatch.mockRejectedValue(wrappedErr);

    const wrapper = newWrapper();
    await wrapper.setData({ code: "123456" });

    await wrapper.find("button[data-test=confirm-btn").trigger("click");
    await wrapper.vm.$nextTick();

    const error = wrapper.find("span[data-test=network-error]");
    expect(error.text()).toBe(wrappedErr.message);
    expect(router.replace).not.toHaveBeenCalled();
  });

  it("displays unwrapped network errors", async () => {
    const unwrappedErr = "Unwrapped error returned";
    store.dispatch.mockRejectedValue(unwrappedErr);

    const wrapper = newWrapper();
    await wrapper.setData({ code: "123456" });

    await wrapper.find("button[data-test=confirm-btn").trigger("click");
    await wrapper.vm.$nextTick();

    const error = wrapper.find("span[data-test=network-error]");
    expect(error.text()).toBe(unwrappedErr);
    expect(router.replace).not.toHaveBeenCalled();
  });

  it("does not lock out confirms after recoverable error", async () => {
    const wrappedErr = {
      code: "RecoverableException",
      message: "Wrapped error returned",
    };
    store.dispatch.mockRejectedValue(wrappedErr);

    const wrapper = newWrapper();
    await wrapper.setData({ code: "123456" });

    const button = wrapper.find("button[data-test=confirm-btn");
    await button.trigger("click");
    await wrapper.vm.$nextTick();
    await wrapper.vm.$nextTick(); // this second tick is necessary, i dont know why
    expect(button.element.disabled).toBeFalsy();
  });

  it("locks out confirms after an unauthorized exception", async () => {
    const wrappedErr = {
      code: "NotAuthorizedException",
      message: "Wrapped error returned",
    };
    store.dispatch.mockRejectedValue(wrappedErr);

    const wrapper = newWrapper();
    await wrapper.setData({ code: "123456" });

    const button = wrapper.find("button[data-test=confirm-btn");
    await button.trigger("click");
    await wrapper.vm.$nextTick();

    expect(button.element.disabled).toBeTruthy();
  });

  it("displays code errors", async () => {
    const wrapper = newWrapper();

    // make it dirty
    const input = wrapper.find("input[data-test=code-input]");
    await input.trigger("blur");

    // empty
    let errs = wrapper.find("div.v-messages__message");
    expect(errs.text()).toBe("Please enter your verification code.");

    // happy
    await wrapper.setData({ code: "1" });
    errs = wrapper.find("div.v-messages__message");
    expect(errs.exists()).toBeFalsy();
  });
});
