import { createWrapper } from "../../../test-wrapper";
import PayoutsListView from "@/components/payouts/PayoutsListView";
import PayoutsHistory from "@/components/payouts/history/PayoutsHistory";
import PayoutsAvailable from "@/components/payouts/available/PayoutsAvailable";
import payout from "@/store/payouts";
import Vuex from "vuex";

describe("PayoutsListView.vue", () => {
  const store = new Vuex.Store({ modules: { payout } });
  store.dispatch = jest.fn();
  beforeEach(() => {
    store.dispatch.mockReset();
  });
  it("displays the availible payouts", () => {
    store.dispatch.mockResolvedValue({ transactions: [] });

    const wrapper = createWrapper(PayoutsListView, {
      propsData: { initActive: "available" },
      store,
    });

    const available = wrapper.findComponent(PayoutsAvailable);
    expect(available.exists()).toBeTruthy();
  });
  it("displays the history", () => {
    store.dispatch.mockResolvedValue({ transactions: [] });

    const wrapper = createWrapper(PayoutsListView, {
      propsData: { initActive: "history" },
      store,
    });

    const history = wrapper.findComponent(PayoutsHistory);
    expect(history.exists()).toBeTruthy();
  });
});
