import Profile from "@/views/Profile";
import { createWrapper } from "../../test-wrapper";
import Vuex from "vuex";
import { FETCH_USER_SETTINGS } from "@/store/action-types";
import { mockUser } from "@/store/profile-settings/mock-data";
import { PUSH_APP_ERROR } from "@/store/mutation-types";
const store = new Vuex.Store({
  state: {
    profile: {
      profile: {},
    },
  },
});

store.dispatch = jest.fn();
store.commit = jest.fn();

describe("Profile.vue", () => {
  beforeEach(() => {
    store.dispatch.mockReset();
  });

  it("fetches the user data", () => {
    store.dispatch.mockResolvedValue(mockUser);
    createWrapper(Profile, { store });
    expect(store.dispatch).toHaveBeenCalledWith(FETCH_USER_SETTINGS);
  });

  it("pushes an app error when failing to fetch user data", async () => {
    store.dispatch.mockRejectedValue({ msg: "something bad happened" });

    const wrapper = createWrapper(Profile, { store });
    await wrapper.vm.$nextTick();

    expect(store.commit).toHaveBeenCalledWith(
      PUSH_APP_ERROR,
      "There was an error retrieving your contact information"
    );
  });
});
