import { createWrapper } from "../../test-wrapper";
import Payouts from "@/views/Payouts";
import { PUSH_APP_ERROR, SET_SPLIT_BACKGROUND } from "@/store/mutation-types";
import { FETCH_PAYMENT_METHODS } from "@/store/action-types";
import payout from "@/store/payouts";
import Vuex from "vuex";
import paymentMethods from "@/store/payment-methods";

describe("Payouts.vue", () => {
  const store = new Vuex.Store({ modules: { paymentMethods, payout } });

  store.dispatch = jest.fn();
  store.commit = jest.fn();

  beforeEach(() => {
    store.dispatch.mockReset();
    store.commit.mockReset();
  });

  it("commits splitBackground true on mounted", () => {
    store.dispatch.mockResolvedValue([]);
    createWrapper(Payouts, { store });

    expect(store.commit).toHaveBeenCalledWith(SET_SPLIT_BACKGROUND, true);
  });

  it("commits splitBackground false on destroyed", () => {
    store.dispatch.mockResolvedValue([]);
    const wrapper = createWrapper(Payouts, { store });

    wrapper.destroy();

    expect(store.commit).toHaveBeenCalledWith(SET_SPLIT_BACKGROUND, false);
  });

  it("fetches required the payment methods", () => {
    store.dispatch.mockResolvedValue([]);
    createWrapper(Payouts, { store });
    expect(store.dispatch).toHaveBeenCalledWith(FETCH_PAYMENT_METHODS);
  });

  it("displays an error when fetch payment methods fails", async () => {
    store.dispatch.mockResolvedValue([]);
    store.dispatch.mockRejectedValueOnce({
      msg: "something bad happened",
    });

    const wrapper = createWrapper(Payouts, { store });
    await wrapper.vm.$nextTick();

    expect(store.commit).toHaveBeenCalledWith(
      PUSH_APP_ERROR,
      "There was an error retrieving your payment methods"
    );
  });
});
