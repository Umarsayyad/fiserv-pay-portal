import { createWrapper } from "../../test-wrapper";
import Vuex from "vuex";
import NotFound from "@/views/NotFound";

describe("NotFound.vue", () => {
  function newWrapper() {
    const options = {
      store,
      mocks: {
        $store: store,
      },
    };

    return createWrapper(NotFound, options);
  }

  let state;
  let getters;
  let store;

  it("hides the 'return to dashboard' button when not authenticated", () => {
    state = {
      auth: { user: { firstName: "Tyrion", lastName: "Lannister" } },
    };
    getters = { isAuthenticated: () => false };
    store = new Vuex.Store({
      getters,
      state,
    });
    const wrapper = newWrapper();
    const button = wrapper.find("[data-test=dashboard-button]");
    expect(button.exists()).toBeFalsy();
  });

  it("shows the 'return to dashboard' button when authenticated", async () => {
    state = {
      auth: { user: { firstName: "Tyrion", lastName: "Lannister" } },
    };
    getters = { isAuthenticated: () => true };
    store = new Vuex.Store({
      getters,
      state,
    });
    const wrapper = newWrapper();
    const button = wrapper.find("[data-test=dashboard-button]");
    expect(button.exists()).toBeTruthy();
  });
});
