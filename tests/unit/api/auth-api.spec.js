import http from "@/api/clients/auth-client";
import auth from "@/api/auth-api";

jest.mock("@/api/clients/auth-client");

describe("auth-api", () => {
  it("creates a limited access token", async () => {
    const lat = {
      tokenId: "0JkTLwNjvMWJNFSJbIT50xhYJWXZ",
      customerAccessKey:
        "8ed2b366-7aa2-4038-84b2-983886da0118#TID-fc68e4f8-6791-491b-948f-9a4505c87cae+8a7f20907a9a1277017a9c2560e5000d",
      publicKey:
        "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkyOgO+pPJSfOvsI07qicEGaLB/eE1T/D5r+pex8Uk95LZTKTmKGwPbElyl6quGqZ/bUbMTb/kmCg1S6WFSVzn8ZsJh8ZYsvkl74PL58IUq+N+gm47FUItX7EqNERGt2+k8uB3jwDfrfmTh2L20uPS/60PL3dr1XyudgD6KPUE4xK1lg+zmXJhVX3FnmLXE8t0cuMg0W5G/YVrJaEkHgldAc4U6A0RyQoA7nD2aUwSrraRQoeZtcnmOvF1l2tb0FbkDjQAimkj/oVicChhQZXe2HGoKgml4/iwiLfQUuGUon937pxHPgKndMExjaYvL/96ZHP4SpzZBan89nENqSi9QIDAQAB",
      algorithm: "RSA/None/OAEPWithSHA512AndMGF1Padding",
      recipientId: "8a7f20907a9a1277017a9c2560e5000d",
      challenge: "",
      optStatus: "IN",
      optRequired: "true",
      groupName: "statefarm",
    };

    http.post.mockResolvedValue(lat);
    const data = await auth.createLimitedAccessToken("ak");
    expect(data).toBe(lat);
  });
});
