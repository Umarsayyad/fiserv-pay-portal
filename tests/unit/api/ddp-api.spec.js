import http from "@/api/clients/ddp-client";
import ddp from "@/api/ddp-api";

jest.mock("@/api/clients/ddp-client");

describe("ddp-api", () => {
  it("creates an otp", async () => {
    const otp = {
      channel: "EMAIL",
      destination: "vma**************",
      sessionId: "1d59d467-3aa2-43ae-bde1-68bfb410e1b0",
    };

    http.post.mockResolvedValue(otp);
    const data = await ddp.createOtp(
      "tyrion@lannister.house",
      "AUTHENTICATION"
    );
    expect(data).toBe(otp);
  });
});
