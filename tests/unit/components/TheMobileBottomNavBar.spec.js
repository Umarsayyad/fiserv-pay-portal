import Vuex from "vuex";
import router from "@/router";
import { createWrapper } from "../../test-wrapper";
import TheUserMenu from "../../../src/components/TheUserMenu";
import TheMobileBottomNavBar from "@/components/TheMobileBottomNavBar";

jest.mock("@/router");
window.scrollTo = jest.fn();

describe("TheMobileBottomNavBar.vue", () => {
  function newWrapper() {
    const options = {
      store,
      mocks: {
        $store: store,
        $router: router,
        $route: {
          name: "dashboard",
        },
      },
    };

    return createWrapper(TheMobileBottomNavBar, options);
  }

  let store;

  beforeEach(() => {
    router.push.mockReset();
    router.replace.mockReset();
  });

  it("display home and payouts buttons", async () => {
    const state = {
      auth: { user: { firstName: "Tyrion", lastName: "Lannister" } },
    };
    store = new Vuex.Store({
      state,
    });

    const wrapper = newWrapper();
    const home = wrapper.find("span[data-test=mobile-nav-bar-home]");
    expect(home.exists()).toBeTruthy();

    const payouts = wrapper.find("span[data-test=mobile-nav-bar-payouts]");
    expect(payouts.exists()).toBeTruthy();
  });

  it("display the user menu component", async () => {
    const state = {
      auth: { user: { firstName: "Tyrion", lastName: "Lannister" } },
    };
    store = new Vuex.Store({
      state,
    });

    const wrapper = newWrapper();
    const menu = wrapper.findComponent(TheUserMenu);
    expect(menu.exists()).toBeTruthy();
  });

  it("scrolls down, then clicks the home button to scroll back to the top", async () => {
    window.scrollTo.mockClear();
    const state = {
      auth: { user: { firstName: "Tyrion", lastName: "Lannister" } },
    };
    store = new Vuex.Store({
      state,
    });

    const wrapper = newWrapper();
    window.scrollTo(0, 75);
    await wrapper.find("span[data-test=mobile-nav-bar-home]").trigger("click");

    expect(window.scrollX).toBe(0);
    expect(window.scrollY).toBe(0);
  });
});
