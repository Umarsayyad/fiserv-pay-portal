import { RouterLinkStub } from "@vue/test-utils";
import router from "@/router";
import TheFooter from "@/components/TheFooter";
import { createWrapper } from "../../test-wrapper";

jest.mock("@/router");

describe("TheFooter.vue", () => {
  beforeEach(() => {
    router.push.mockReset();
    router.replace.mockReset();
  });

  it("displays correct year in trademark", () => {
    const wrapper = createWrapper(TheFooter, {
      mocks: {
        $router: router,
      },
    });
    const year = `${new Date().getFullYear()}`;
    const foundTrademark = wrapper.find("div[data-test=watermark]");
    expect(foundTrademark.element.textContent).toContain(year);
  });

  it("renders a term  &  privacy ", () => {
    const wrapper = createWrapper(TheFooter, {
      mocks: {
        $router: router,
      },
      stubs: {
        RouterLink: RouterLinkStub,
      },
    });

    const termNCondition = wrapper.find("[data-test=link-to-terms-conditions]");
    expect(termNCondition.props().to.name).toEqual("terms");
    const privacyPolicy = wrapper.find("[data-test=link-to-privacy-policy]");
    expect(privacyPolicy.props().to.name).toEqual("privacy-policy");
  });
});
