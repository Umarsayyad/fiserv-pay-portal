import { createWrapper } from "../../../test-wrapper";
import DashboardPayoutTile from "../../../../src/components/dashboard/DashboardPayoutTile";

describe("DashboardPayoutTile.vue", () => {
  const loadingSelector = "div[data-test=tile-loading]";
  const titleSelector = "div[data-test=tile-title]";
  const dataSelector = "div[data-test=tile-data]";
  const acceptActionSelector = "div[data-test=tile-accept-button]";
  const dashboardTiles = "div[data-test=dashboard-tiles]";

  it("displays loader", async () => {
    const wrapper = createWrapper(DashboardPayoutTile, {
      propsData: { loading: true, title: "", data: "", icon: "", toAction: "" },
    });

    const loading = wrapper.find(loadingSelector);
    expect(loading.exists()).toBeTruthy();

    const skeletons = wrapper.findAll("div[data-test=loading-item]");
    expect(skeletons.exists()).toBeTruthy();

    const tiles = wrapper.find(dashboardTiles);
    expect(tiles.exists()).toBeFalsy();
  });

  it("hide loader and display tiles info", async () => {
    const tileData = "5";
    const tileTitle = "Available Payouts";
    const tileAcceptButton = true;
    const tileLoading = false;

    const wrapper = createWrapper(DashboardPayoutTile, {
      propsData: {
        loading: tileLoading,
        data: tileData,
        title: tileTitle,
        displayAcceptAction: tileAcceptButton,
        icon: "",
        toAction: "",
      },
    });

    const loading = wrapper.find(loadingSelector);
    expect(loading.exists()).toBeFalsy();

    const title = wrapper.find(titleSelector);
    expect(title.exists()).toBeTruthy();
    expect(title.text()).toBe(tileTitle);

    const data = wrapper.find(dataSelector);
    expect(data.exists()).toBeTruthy();
    expect(data.text()).toBe(tileData);

    const displayAcceptAction = wrapper.find(acceptActionSelector);
    expect(displayAcceptAction.exists()).toBeTruthy();
  });

  it("hide loader and display current payout", async () => {
    const tileData = "6";
    const tileTitle = "Current Payout Amount";
    const tileAcceptButton = true;
    const tileLoading = false;
    const tileToAction = { name: "payouts" };
    const mockRouter = {
      push: jest.fn(),
    };
    const wrapper = createWrapper(DashboardPayoutTile, {
      propsData: {
        loading: tileLoading,
        data: tileData,
        title: tileTitle,
        displayAcceptAction: tileAcceptButton,
        icon: "",
        toAction: "payouts",
      },

      mocks: {
        $router: mockRouter,
      },
    });

    const loading = wrapper.find(loadingSelector);
    expect(loading.exists()).toBeFalsy();

    const title = wrapper.find(titleSelector);
    expect(title.exists()).toBeTruthy();
    expect(title.text()).toBe(tileTitle);

    const data = wrapper.find(dataSelector);
    expect(data.exists()).toBeTruthy();
    expect(data.text()).toBe(tileData);

    const displayAcceptAction = wrapper.find(acceptActionSelector);
    expect(displayAcceptAction.exists()).toBeTruthy();

    await wrapper.find("button").trigger("click");

    expect(mockRouter.push).toHaveBeenCalledTimes(1);
    expect(mockRouter.push).toHaveBeenCalledWith(tileToAction);
  });

  it("should not show action button from current payout if amount 0", async () => {
    const tileData = "0";
    const tileTitle = "Current Payout Amount";
    const tileAcceptButton = false;
    const tileLoading = false;

    const wrapper = createWrapper(DashboardPayoutTile, {
      propsData: {
        loading: tileLoading,
        data: tileData,
        title: tileTitle,
        displayAcceptAction: tileAcceptButton,
        icon: "",
      },
    });

    const loading = wrapper.find(loadingSelector);
    expect(loading.exists()).toBeFalsy();

    const title = wrapper.find(titleSelector);
    expect(title.exists()).toBeTruthy();
    expect(title.text()).toBe(tileTitle);

    const data = wrapper.find(dataSelector);
    expect(data.exists()).toBeTruthy();
    expect(data.text()).toBe(tileData);

    const displayAcceptAction = wrapper.find(acceptActionSelector);
    expect(displayAcceptAction.exists()).toBeFalsy();
  });
});
