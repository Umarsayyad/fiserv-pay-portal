import { createWrapper } from "../../../../test-wrapper";
import RecentActivities from "@/components/dashboard/activities/RecentActivities";

describe("RecentActivities.vue", () => {
  it("displays a title for the recent activity", async () => {
    const wrapper = createWrapper(RecentActivities);

    const greeting = wrapper.find("div[data-test=recent-activity-title]");
    expect(greeting.text()).toBe("Recent Activity");
  });
});
