import { createWrapper } from "../../../../test-wrapper";
import DashboardPayoutsListItem from "@/components/dashboard/payouts-list/DashboardPayoutsListItem";
import { mockAvailablePayouts } from "@/store/payouts/mock-data";
import { currency } from "@/filters/filters";
import Vuex from "vuex";
import merchantStore from "@/store/merchant";

describe("DashboardPayoutsListItem.vue", () => {
  it("renders the payout", () => {
    const payout = mockAvailablePayouts.transactions[0];
    const store = new Vuex.Store({ modules: { merchant: merchantStore } });
    const wrapper = createWrapper(DashboardPayoutsListItem, {
      store,
      propsData: { payout: payout },
    });

    const merchantName = wrapper.find("div[data-test=merchant-name]");
    const merchant = payout.recipient[0];
    expect(merchantName.text()).toBe(merchant.doingBusinessAs);

    const expiryDate = wrapper.find("div[data-test=expiry-date]");
    expect(expiryDate.text()).toBe(payout.expiryDate);

    const amount = wrapper.find("div[data-test=amount");
    expect(amount.text()).toBe(
      currency(
        payout.totalTransactionAmount.total,
        payout.totalTransactionAmount.currency
      )
    );
  });
});
