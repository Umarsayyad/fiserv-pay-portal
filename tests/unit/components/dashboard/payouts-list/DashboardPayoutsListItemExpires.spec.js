import { createWrapper } from "../../../../test-wrapper";
import DashboardPayoutsListItemExpires from "@/components/dashboard/payouts-list/DashboardPayoutsListItemExpires";
import { add, format } from "date-fns";

describe("DashboardPayoutsListItemExpires.vue", () => {
  const dateFmt = "MM/dd/yyyy";

  const today = new Date();
  const todayFmt = format(today, dateFmt);

  const tomorrow = add(today, { days: 1 });
  const tomorrowFmt = format(tomorrow, dateFmt);

  it("displays the expiry date", () => {
    const wrapper = createWrapper(DashboardPayoutsListItemExpires, {
      propsData: { expiryDate: tomorrowFmt },
    });

    const expiryDate = wrapper.find("div[data-test=expiry-date]");
    expect(expiryDate.text()).toBe(tomorrowFmt);
    expect(expiryDate.classes()).not.toContain("error--text");
  });

  it("displays the expiry date in red if expiring today", () => {
    const wrapper = createWrapper(DashboardPayoutsListItemExpires, {
      propsData: { expiryDate: todayFmt },
    });

    const expiryDate = wrapper.find("div[data-test=expiry-date]");
    expect(expiryDate.text()).toBe(todayFmt);
    expect(expiryDate.classes()).toContain("error--text");
  });
});
