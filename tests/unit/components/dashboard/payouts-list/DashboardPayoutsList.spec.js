import { createWrapper } from "../../../../test-wrapper";
import DashboardPayoutsList from "@/components/dashboard/payouts-list/DashboardPayoutsList";
import { mockAvailablePayouts } from "@/store/payouts/mock-data";
import DashboardPayoutsListItem from "@/components/dashboard/payouts-list/DashboardPayoutsListItem";
import Vuex from "vuex";
import merchant from "@/store/merchant";

describe("DashboardPayoutsList.vue", () => {
  const loadingSelector = "div[data-test=available-payouts-list-loader]";
  const payoutsListSelector = "div[data-test=payouts-list";
  const noPayoutsSelector = "div[data-test=no-payouts]";
  const payoutsListItemsSelector = "div[data-test=payouts-list-items]";

  const store = new Vuex.Store({ modules: { merchant } });

  function newWrapper(props) {
    return createWrapper(DashboardPayoutsList, {
      store,
      propsData: props,
    });
  }

  it("displays the loader", () => {
    const displayCount = 2;
    const wrapper = newWrapper({ loading: true, displayCount });

    const loading = wrapper.find(loadingSelector);
    expect(loading.exists()).toBeTruthy();

    const skeletons = wrapper.findAll("div[data-test=loading-item]");
    expect(skeletons.length).toBe(displayCount);

    const payoutsList = wrapper.find(payoutsListSelector);
    expect(payoutsList.exists()).toBeFalsy();
  });

  it("displays the payouts list", () => {
    const wrapper = newWrapper();

    const payoutsList = wrapper.find(payoutsListSelector);
    expect(payoutsList.exists()).toBeTruthy();

    const loading = wrapper.find(loadingSelector);
    expect(loading.exists()).toBeFalsy();
  });

  it("displays the payout count", () => {
    const payouts = mockAvailablePayouts.transactions;
    const wrapper = newWrapper({ payouts });

    const payoutCount = wrapper.find("div[data-test=payout-count]");
    expect(payoutCount.text()).toBe(`${payouts.length}`);
  });

  it("displays no payouts indication if there are no payouts", () => {
    const wrapper = newWrapper({ payouts: [] });

    const noPayouts = wrapper.find(noPayoutsSelector);
    expect(noPayouts.exists()).toBeTruthy();

    const listItems = wrapper.find(payoutsListItemsSelector);
    expect(listItems.exists()).toBeFalsy();
  });

  it("displays the payouts list items", () => {
    const payouts = mockAvailablePayouts.transactions;
    const wrapper = newWrapper({ payouts });

    const listItems = wrapper.find(payoutsListItemsSelector);
    expect(listItems.exists()).toBeTruthy();

    const noPayouts = wrapper.find(noPayoutsSelector);
    expect(noPayouts.exists()).toBeFalsy();
  });

  it("sorts the payout list items", () => {
    const payouts = mockAvailablePayouts.transactions;
    const displayCount = 2;
    const wrapper = newWrapper({ payouts, displayCount });

    const listItems = wrapper.findAllComponents(DashboardPayoutsListItem);
    expect(listItems.length).toBe(displayCount);

    const expiring = payouts[1];
    const firstExpiryDate = listItems.at(0).find("div[data-test=expiry-date]");
    expect(firstExpiryDate.text()).toBe(expiring.expiryDate);
  });
});
