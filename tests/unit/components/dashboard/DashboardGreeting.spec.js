import Vuex from "vuex";
import { createWrapper } from "../../../test-wrapper";
import DashboardGreeting from "@/components/dashboard/DashboardGreeting";

describe("DashboardGreeting.vue", () => {
  it("displays a greeting for the user", async () => {
    const store = new Vuex.Store({
      state: { auth: { user: { firstName: "TYRION" } } },
    });
    const wrapper = createWrapper(DashboardGreeting, { store });

    const greeting = wrapper.find("div[data-test=dashboard-greeting]");
    expect(greeting.text()).toBe("Welcome, Tyrion!");
  });
});
