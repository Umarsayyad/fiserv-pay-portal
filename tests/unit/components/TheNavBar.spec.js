import Vuex from "vuex";
import TheNavBar from "@/components/TheNavBar";
import TheUserMenu from "@/components/TheUserMenu";
import { createWrapper } from "../../test-wrapper";

describe("TheNavBar.vue", () => {
  function newWrapper() {
    const options = {
      store,
      mocks: {
        $store: store,
        $route: { path: "/" },
      },
      stubs: ["v-tab"],
    };

    return createWrapper(TheNavBar, options);
  }

  let state;
  let getters;
  let store;

  it("hides the user menu when not authenticated", () => {
    state = {
      auth: { user: { firstName: "Tyrion", lastName: "Lannister" } },
    };
    getters = { isAuthenticated: () => false };
    store = new Vuex.Store({
      getters,
      state,
    });

    const wrapper = newWrapper();
    const menu = wrapper.findComponent(TheUserMenu);
    expect(menu.exists()).toBeFalsy();
  });

  it("displays the user menu when authenticated", () => {
    state = {
      auth: { user: { firstName: "Tyrion", lastName: "Lannister" } },
    };
    getters = { isAuthenticated: () => true };
    store = new Vuex.Store({
      getters,
      state,
    });

    const wrapper = newWrapper();
    const menu = wrapper.findComponent(TheUserMenu);
    expect(menu.exists()).toBeTruthy();
  });
});
