import { createWrapper } from "../../../test-wrapper";
import PayoutByMerchant from "@/components/payouts-by-merchant/PayoutByMerchant";
import { mockTotalPayoutAmountsByMerchant } from "@/store/payouts/mock-data";
import Vuex from "vuex";
import merchantStore from "@/store/merchant";

describe("PayoutByMerchant.vue", () => {
  const legendItemSelector = "div[data-test=legend-item]";
  const legendMerchantSelector = "div[data-test=legend-merchant]";
  const legendPercentSelector = "div[data-test=legend-percent]";

  it("sorts the merchants by total desc", () => {
    const lannisterInc = {
      groupName: "lannister",
      doingBusinessAs: "Lannister Inc",
      totalPayoutsAmount: {
        total: 5000,
        currency: "USD",
      },
    };
    const store = new Vuex.Store({ modules: { merchant: merchantStore } });
    const wrapper = createWrapper(PayoutByMerchant, {
      store,
      propsData: {
        totals: [...mockTotalPayoutAmountsByMerchant, lannisterInc],
      },
      stubs: ["brand-logo"],
    });
    const legendItems = wrapper.findAll(legendItemSelector);
    expect(legendItems.length).toBe(3);

    const first = legendItems.at(0);
    let merchant = first.find(legendMerchantSelector);
    expect(merchant.text()).toBe(lannisterInc.doingBusinessAs);
    let percent = first.find(legendPercentSelector);
    expect(percent.text()).toBe("77%");

    const second = legendItems.at(1);
    merchant = second.find(legendMerchantSelector);
    expect(merchant.text()).toBe("State Farm");
    percent = second.find(legendPercentSelector);
    expect(percent.text()).toBe("15%");

    const third = legendItems.at(2);
    merchant = third.find(legendMerchantSelector);
    expect(merchant.text()).toBe("Lyft");
    percent = third.find(legendPercentSelector);
    expect(percent.text()).toBe("8%");
  });

  it("consolidates lower lifetime contributers when there are not enough colors", () => {
    const lannisterInc = {
      groupName: "lannister",
      doingBusinessAs: "Lannister Inc",
      totalPayoutsAmount: {
        total: 5000,
        currency: "USD",
      },
    };
    const wrapper = createWrapper(PayoutByMerchant, {
      propsData: {
        totals: [...mockTotalPayoutAmountsByMerchant, lannisterInc],
      },
      data() {
        return {
          colors: ["blue", "green"],
        };
      },
      stubs: ["brand-logo"],
    });

    const legendItems = wrapper.findAll(legendItemSelector);
    expect(legendItems.length).toBe(2);

    const first = legendItems.at(0);
    let merchant = first.find(legendMerchantSelector);
    expect(merchant.text()).toBe(lannisterInc.doingBusinessAs);
    let percent = first.find(legendPercentSelector);
    expect(percent.text()).toBe("77%");

    const second = legendItems.at(1);
    merchant = second.find(legendMerchantSelector);
    expect(merchant.text()).toBe("Other");
    percent = second.find(legendPercentSelector);
    expect(percent.text()).toBe("23%");
  });

  it("skips sort and consolidation if empty", () => {
    const wrapper = createWrapper(PayoutByMerchant, {
      propsData: {
        totals: [],
      },
      stubs: ["brand-logo"],
    });

    const items = wrapper.findAll(legendItemSelector);
    expect(items.length).toBe(0);
  });
});
