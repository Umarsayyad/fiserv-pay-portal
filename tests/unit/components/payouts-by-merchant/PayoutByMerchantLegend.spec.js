import { createWrapper } from "../../../test-wrapper";
import PayoutByMerchantLegend from "@/components/payouts-by-merchant/PayoutByMerchantLegend";
import { mockTotalPayoutAmountsByMerchant } from "@/store/payouts/mock-data";
import Vuex from "vuex";
import merchant from "@/store/merchant";

describe("PayoutByMerchantLegend.vue", () => {
  it("renders the legend", () => {
    const store = new Vuex.Store({ modules: { merchant } });
    const wrapper = createWrapper(PayoutByMerchantLegend, {
      store,
      propsData: {
        byMerchantTotals: mockTotalPayoutAmountsByMerchant,
        colors: ["blue", "green"],
        overallSum: { total: 1500, currency: "USD" },
      },
    });

    const items = wrapper.findAll("div[data-test=legend-item]");
    expect(items.length).toBe(mockTotalPayoutAmountsByMerchant.length);

    const expectations = [
      {
        color: "blue",
        merchant: "State Farm",
        percent: "67%",
      },
      {
        color: "green",
        merchant: "Lyft",
        percent: "33%",
      },
    ];

    for (let i = 0; i < items.length; i++) {
      const item = items.at(i);
      const expectation = expectations[i];

      const color = item.find("div[data-test=legend-color-indicator]");
      expect(color.attributes().style).toBe(
        `background-color: ${expectation.color};`
      );

      const logo = item.find("div[data-test=legend-logo");
      expect(logo.exists()).toBeTruthy();

      const merchant = item.find("div[data-test=legend-merchant");
      expect(merchant.text()).toBe(expectation.merchant);

      const percent = item.find("div[data-test=legend-percent]");
      expect(percent.text()).toBe(expectation.percent);
    }
  });

  it("hides the logo for the Other", () => {
    const wrapper = createWrapper(PayoutByMerchantLegend, {
      propsData: {
        byMerchantTotals: [
          {
            groupName: "other",
            doingBusinessAs: "Other",
            totalPayoutsAmount: {
              total: 1000,
              currency: "USD",
            },
          },
        ],
        colors: ["blue", "green"],
        overallSum: { total: 1500, currency: "USD" },
      },
    });

    const other = wrapper.find("div[data-test=legend-item]");
    expect(other.exists()).toBeTruthy();

    const logo = other.find("div[data-test=legend-logo");
    expect(logo.exists()).toBeFalsy();
  });
});
