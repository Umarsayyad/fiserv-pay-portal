import { createWrapper } from "../../../test-wrapper";
import PayoutByMerchantDoughnut from "@/components/payouts-by-merchant/PayoutByMerchantDoughnut";
import { mockTotalPayoutAmountsByMerchant } from "@/store/payouts/mock-data";

describe("PayoutByMerchantDoughnut.vue", () => {
  it("computes the graph data", () => {
    const wrapper = createWrapper(PayoutByMerchantDoughnut, {
      propsData: {
        byMerchantTotals: mockTotalPayoutAmountsByMerchant,
        colors: ["blue", "green"],
        overallSum: { total: 1500, currency: "USD" },
      },
    });

    const graph = wrapper.vm.graph;
    expect(graph.data.labels).toStrictEqual(["State Farm", "Lyft"]);
    expect(graph.data.datasets[0].data).toStrictEqual([1000, 500]);
    expect(graph.data.datasets[0].backgroundColor).toStrictEqual([
      "blue",
      "green",
    ]);

    const tooltipCallbacks = graph.options.plugins.tooltip.callbacks;

    const label = tooltipCallbacks.label({ label: "State Farm" });
    expect(label).toBe("State Farm");

    const footer = tooltipCallbacks.footer([{ raw: 1000 }]);
    expect(footer).toBe("$1,000.00");
  });

  it("updates the chart when the graph data changes", async () => {
    const wrapper = createWrapper(PayoutByMerchantDoughnut, {
      propsData: {
        byMerchantTotals: mockTotalPayoutAmountsByMerchant,
        colors: ["blue", "green"],
        overallSum: { total: 1500, currency: "USD" },
      },
    });

    const chartUpdate = jest.spyOn(wrapper.vm.chart, "update");

    await wrapper.setProps({ colors: ["red", "green"] });

    expect(chartUpdate).toHaveBeenCalled();
  });

  it("doesn't update the chart if there is no data", async () => {
    const wrapper = createWrapper(PayoutByMerchantDoughnut, {
      propsData: {
        byMerchantTotals: mockTotalPayoutAmountsByMerchant,
        colors: ["blue", "green"],
        overallSum: { total: 1500, currency: "USD" },
      },
    });

    const chartUpdate = jest.spyOn(wrapper.vm.chart, "update");

    await wrapper.setProps({ byMerchantTotals: [] });

    expect(chartUpdate).not.toHaveBeenCalled();
  });

  it("creates a new chart if one does not exist", async () => {
    const wrapper = createWrapper(PayoutByMerchantDoughnut, {
      propsData: {
        byMerchantTotals: mockTotalPayoutAmountsByMerchant,
        colors: ["blue", "green"],
        overallSum: { total: 1500, currency: "USD" },
      },
    });

    wrapper.vm.chart.destroy();
    await wrapper.setData({ chart: null });
    expect(wrapper.vm.chart).toBeNull();

    await wrapper.setProps({ colors: ["red", "green"] });

    expect(wrapper.vm.chart).not.toBeNull();
  });
});
