import { createWrapper } from "../../test-wrapper";
import MerchantLogo from "@/components/MerchantLogo";
import merchant from "@/store/merchant";
import Vuex from "vuex";

describe("MerchantLogo.vue", () => {
  const defaultStore = new Vuex.Store({ modules: { merchant } });

  function newWrapper(props, store) {
    const s = store ?? defaultStore;
    return createWrapper(MerchantLogo, { store: s, propsData: props });
  }

  it("displays the logo", () => {
    const wrapper = newWrapper({ merchant: "statefarm", size: "3rem" });
    expect(wrapper.element.style.getPropertyValue("--fill")).toBe(
      merchant.state().merchants.statefarm.color
    );
    expect(wrapper.element.style.getPropertyValue("width")).toBe("3rem");
    expect(wrapper.element.style.getPropertyValue("height")).toBe("3rem");
    expect(wrapper.element.style.getPropertyValue("min-width")).toBe("3rem");

    const logo = wrapper.find("svg[data-test=merchant-logo-svg]");
    expect(logo.exists()).toBeTruthy();

    const fallback = wrapper.find("span[data-test=merchant-logo-fallback]");
    expect(fallback.exists()).toBeFalsy();
  });

  it("displays the fallback icon", () => {
    const wrapper = newWrapper({ merchant: "something-bad" });
    expect(wrapper.element.style.getPropertyValue("--fill")).toBe(
      wrapper.vm.$vuetify.theme.themes.light.secondary
    );

    const logo = wrapper.find("svg[data-test=merchant-logo-svg]");
    expect(logo.exists()).toBeFalsy();

    const fallback = wrapper.find("span[data-test=merchant-logo-fallback]");
    expect(fallback.exists()).toBeTruthy();
    expect(fallback.props("color")).toBe("secondary");
  });

  it("displays a logo with a backdrop", () => {
    const wrapper = newWrapper({ merchant: "statefarm", backdrop: true });

    expect(wrapper.classes()).toContain("backdrop");
    expect(wrapper.element.style.getPropertyValue("--backdrop")).toBe(
      merchant.state().merchants.statefarm.color
    );
    expect(wrapper.element.style.getPropertyValue("--fill")).toBe("#ffffff");
  });

  it("displays the fallback icon with a secondary backdrop", () => {
    const wrapper = newWrapper({ merchant: "something_bad", backdrop: true });

    expect(wrapper.classes()).toContain("backdrop");
    expect(wrapper.element.style.getPropertyValue("--backdrop")).toBe(
      wrapper.vm.$vuetify.theme.themes.light.secondary
    );

    const fallback = wrapper.find("span[data-test=merchant-logo-fallback]");
    expect(fallback.exists()).toBeTruthy();
    expect(fallback.props("color")).toBe("white");
  });

  it("displays without padding", () => {
    const wrapper = newWrapper({ merchant: "statefarm", noPadding: true });
    expect(wrapper.classes()).toContain("pa-0");
  });

  it("uses white if unable to find merchant color", () => {
    const wrapper = newWrapper(
      { merchant: "statefarm" },
      new Vuex.Store({ modules: { merchant: { state: { merchants: {} } } } })
    );

    const logo = wrapper.find("svg[data-test=merchant-logo-svg]");
    expect(logo.exists()).toBeTruthy();
    expect(wrapper.element.style.getPropertyValue("--fill")).toBe("#ffffff");
  });
});
