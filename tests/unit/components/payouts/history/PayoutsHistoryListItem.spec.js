import { createWrapper } from "../../../../test-wrapper";
import PayoutsHistoryListItem from "@/components/payouts/history/PayoutsHistoryListItem";
import { mockPayoutHistory } from "@/store/payouts/mock-data";
import { currency } from "@/filters/filters";
import MerchantLogo from "@/components/MerchantLogo";
import Vuex from "vuex";
import merchant from "@/store/merchant";

describe("PayoutsHistoryListItem.vue", () => {
  const store = new Vuex.Store({ modules: { merchant } });

  function newWrapper(props) {
    return createWrapper(PayoutsHistoryListItem, {
      store,
      propsData: props,
    });
  }

  it("displays the logo, merchant, and amount", () => {
    const payout = mockPayoutHistory.transactions[0];
    const wrapper = newWrapper({ payout });

    const logo = wrapper.findComponent(MerchantLogo);
    expect(logo.exists()).toBeTruthy();

    const merchant = wrapper.find("div[data-test=history-item-merchant]");
    expect(merchant.text()).toBe(payout.recipient[0].doingBusinessAs);

    const amount = wrapper.find("div[data-test=history-item-amount]");
    expect(amount.text()).toBe(currency(payout.totalTransactionAmount.total));
  });

  it("displays the correct status badge", () => {
    const success = mockPayoutHistory.transactions[0];
    let wrapper = newWrapper({ payout: success });

    const badgeSelector = "div[data-test=history-item-status-badge]";

    let badge = wrapper.find(badgeSelector);
    expect(badge.classes()).toContain("success");

    const err = mockPayoutHistory.transactions[1];
    wrapper = newWrapper({ payout: err });

    badge = wrapper.find(badgeSelector);
    expect(badge.classes()).toContain("error");
  });

  it("displays the correct status", () => {
    const success = mockPayoutHistory.transactions[0];
    let wrapper = newWrapper({ payout: success });

    const selector = "div[data-test=history-item-status]";

    let status = wrapper.find(selector);
    expect(status.classes()).toContain("secondary--text");
    expect(status.text()).toBe("Completed");

    const err = mockPayoutHistory.transactions[1];
    wrapper = newWrapper({ payout: err });

    status = wrapper.find(selector);
    expect(status.classes()).toContain("error--text");
    expect(status.text()).toBe("Accepted");
  });
});
