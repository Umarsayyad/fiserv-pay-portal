import { createWrapper } from "../../../../test-wrapper";
import PayoutsHistoryPagination from "@/components/payouts/history/PayoutsHistoryPagination";

describe("PayoutsHistoryPagination.vue", () => {
  it("works with the defaults", () => {
    const wrapper = createWrapper(PayoutsHistoryPagination);
    expect(wrapper.exists()).toBeTruthy();
  });

  it("displays the correct status", async () => {
    const pages = {
      currentPage: 1,
      firstPage: true,
      lastPage: false,
      totalRecords: 25,
      currentPageRecords: 10,
    };

    const wrapper = createWrapper(PayoutsHistoryPagination, {
      propsData: {
        pages,
      },
    });

    const status = wrapper.find("div[data-test=pagination-status]");
    expect(status.text()).toBe("1 - 10 of 25 items");

    await wrapper.setProps({ pages: { ...pages, currentPage: 2 } });
    expect(status.text()).toBe("11 - 20 of 25 items");

    await wrapper.setProps({
      pages: { ...pages, currentPage: 3, currentPageRecords: 5 },
    });
    expect(status.text()).toBe("21 - 25 of 25 items");
  });

  it("disables the previous button on the first page", () => {
    const pages = {
      currentPage: 1,
      firstPage: true,
      lastPage: false,
      totalRecords: 25,
      currentPageRecords: 10,
    };

    const wrapper = createWrapper(PayoutsHistoryPagination, {
      propsData: {
        pages,
      },
    });

    const previous = wrapper.find("button[data-test=pagination-previous-btn]");
    expect(previous.element.disabled).toBeTruthy();
  });

  it("enables the previous button when not on the first page", () => {
    const pages = {
      currentPage: 2,
      firstPage: false,
      lastPage: false,
      totalRecords: 25,
      currentPageRecords: 10,
    };

    const wrapper = createWrapper(PayoutsHistoryPagination, {
      propsData: {
        pages,
      },
    });

    const previous = wrapper.find("button[data-test=pagination-previous-btn]");
    expect(previous.element.disabled).toBeFalsy();
  });

  it("disables the next button on the last page", () => {
    const pages = {
      currentPage: 3,
      firstPage: false,
      lastPage: true,
      totalRecords: 25,
      currentPageRecords: 5,
    };

    const wrapper = createWrapper(PayoutsHistoryPagination, {
      propsData: {
        pages,
      },
    });

    const next = wrapper.find("button[data-test=pagination-next-btn]");
    expect(next.element.disabled).toBeTruthy();
  });

  it("enables the next button when not on the last page", () => {
    const pages = {
      currentPage: 2,
      firstPage: false,
      lastPage: false,
      totalRecords: 25,
      currentPageRecords: 10,
    };

    const wrapper = createWrapper(PayoutsHistoryPagination, {
      propsData: {
        pages,
      },
    });

    const next = wrapper.find("button[data-test=pagination-next-btn]");
    expect(next.element.disabled).toBeFalsy();
  });

  it("emits changesize on size change", async () => {
    const wrapper = createWrapper(PayoutsHistoryPagination);
    const select = wrapper.find("input[data-test=pagination-size-select]");
    await select.trigger("click");

    const options = wrapper.findAll("div.v-list-item");
    await options.at(1).trigger("click");

    expect(wrapper.emitted().changesize).toBeTruthy();
    expect(wrapper.emitted().changesize[0][0]).toBe(25);
  });
});
