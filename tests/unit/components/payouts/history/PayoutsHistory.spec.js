import { createWrapper } from "../../../../test-wrapper";
import PayoutsHistory from "@/components/payouts/history/PayoutsHistory";
import { mockPayoutHistory } from "@/store/payouts/mock-data";
import PayoutsHistoryListItem from "@/components/payouts/history/PayoutsHistoryListItem";
import { FETCH_PAYOUT_HISTORY } from "@/store/action-types";
import PayoutsHistoryPagination from "@/components/payouts/history/PayoutsHistoryPagination";
import Vuex from "vuex";
import merchant from "@/store/merchant";

describe("PayoutsHistory.vue", () => {
  const store = new Vuex.Store({ modules: { merchant } });
  store.dispatch = jest.fn();

  function newWrapper() {
    return createWrapper(PayoutsHistory, { store });
  }

  beforeEach(() => {
    store.dispatch.mockReset();
  });

  it("displays the history list", async () => {
    store.dispatch.mockResolvedValue(mockPayoutHistory);

    const wrapper = newWrapper();
    expect(store.dispatch).toHaveBeenCalledWith(FETCH_PAYOUT_HISTORY, {
      page: 1,
      size: 10,
    });
    await wrapper.vm.$nextTick();
    await wrapper.vm.$nextTick();

    const items = wrapper.findAllComponents(PayoutsHistoryListItem);
    expect(items.length).toBe(mockPayoutHistory.transactions.length);
  });

  it("displays the loader", () => {
    store.dispatch.mockResolvedValue(mockPayoutHistory);

    const wrapper = newWrapper();
    expect(store.dispatch).toHaveBeenCalledWith(FETCH_PAYOUT_HISTORY, {
      page: 1,
      size: 10,
    });

    const loader = wrapper.find("div[data-test=history-loader]");
    expect(loader.isVisible()).toBeTruthy();
  });

  it("re-fetches the history on size change", async () => {
    store.dispatch.mockResolvedValue(mockPayoutHistory);

    const wrapper = newWrapper();
    expect(store.dispatch).toHaveBeenCalledWith(FETCH_PAYOUT_HISTORY, {
      page: 1,
      size: 10,
    });
    await wrapper.vm.$nextTick();

    const pagination = wrapper.findComponent(PayoutsHistoryPagination);
    await pagination.vm.$emit("changesize", 25);

    expect(store.dispatch).toHaveBeenCalledWith(FETCH_PAYOUT_HISTORY, {
      page: 1,
      size: 25,
    });
  });
});
