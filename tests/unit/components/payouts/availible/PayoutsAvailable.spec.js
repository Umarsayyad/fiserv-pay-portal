import { createWrapper } from "../../../../test-wrapper";
import PayoutsAvailable from "@/components/payouts/available/PayoutsAvailable";
import { mockAvailablePayouts } from "@/store/payouts/mock-data";
import Vuex from "vuex";
import merchant from "@/store/merchant";
import PayoutsAvailableListItem from "@/components/payouts/available/PayoutsAvailableListItem";
import { FETCH_AVAILABLE_PAYOUTS } from "@/store/action-types";

describe("PayoutsAvailable.vue", () => {
  const store = new Vuex.Store({ modules: { merchant } });
  store.dispatch = jest.fn();

  beforeEach(() => {
    store.dispatch.mockReset();
  });

  it("displays the Available list", async () => {
    store.dispatch.mockResolvedValue(mockAvailablePayouts);

    const wrapper = createWrapper(PayoutsAvailable, { store });
    expect(store.dispatch).toHaveBeenCalledWith(FETCH_AVAILABLE_PAYOUTS, {
      page: 1,
      size: 10,
    });
    await wrapper.vm.$nextTick();
    await wrapper.vm.$nextTick();

    const items = wrapper.findAllComponents(PayoutsAvailableListItem);
    expect(items.length).toBe(mockAvailablePayouts.transactions.length);
  });

  it("displays the loader", () => {
    store.dispatch.mockResolvedValue(mockAvailablePayouts);

    const wrapper = createWrapper(PayoutsAvailable, { store });
    expect(store.dispatch).toHaveBeenCalledWith(FETCH_AVAILABLE_PAYOUTS, {
      page: 1,
      size: 10,
    });

    const loader = wrapper.find("div[data-test=available-loader]");
    expect(loader.isVisible()).toBeTruthy();
  });

  it("re-fetches the Available on size change", async () => {
    store.dispatch.mockResolvedValue(mockAvailablePayouts);

    const wrapper = createWrapper(PayoutsAvailable, { store });
    expect(store.dispatch).toHaveBeenCalledWith(FETCH_AVAILABLE_PAYOUTS, {
      page: 1,
      size: 10,
    });
    await wrapper.vm.$nextTick();

    expect(store.dispatch).toHaveBeenCalledWith(FETCH_AVAILABLE_PAYOUTS, {
      page: 1,
      size: 10,
    });
  });
});
