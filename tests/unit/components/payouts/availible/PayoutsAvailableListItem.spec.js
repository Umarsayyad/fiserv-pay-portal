import { createWrapper } from "../../../../test-wrapper";
import PayoutsAvailableListItem from "@/components/payouts/available/PayoutsAvailableListItem";
import { mockAvailablePayouts } from "@/store/payouts/mock-data";
import MerchantLogo from "@/components/MerchantLogo";
import { currency } from "@/filters/filters";
import Vuex from "vuex";
import merchant from "@/store/merchant";

describe("PayoutsAvailableListItem.vue", () => {
  const store = new Vuex.Store({ modules: { merchant } });
  function newWrapper(props) {
    return createWrapper(PayoutsAvailableListItem, {
      store,
      propsData: props,
    });
  }

  it("displays the logo, merchant, and amount", () => {
    const payout = mockAvailablePayouts.transactions[0];
    const wrapper = newWrapper({ payout });

    const logo = wrapper.findComponent(MerchantLogo);
    expect(logo.exists()).toBeTruthy();

    const merchant = wrapper.find("div[data-test=available-item-merchant]");
    expect(merchant.text()).toBe(payout.recipient[0].doingBusinessAs);

    const amount = wrapper.find("div[data-test=available-item-amount]");
    expect(amount.text()).toBe(currency(payout.totalTransactionAmount.total));
  });
});
