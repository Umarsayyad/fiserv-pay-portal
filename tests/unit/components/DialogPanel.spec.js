import { createWrapper } from "../../test-wrapper";
import DialogPanel from "@/components/DialogPanel";

describe("DialogPanel.vue", () => {
  it("displays the dialog", async () => {
    const wrapper = createWrapper(DialogPanel, {
      propsData: {
        title: "Add New",
        value: true,
      },
    });

    const title = wrapper.find("div[data-test=dialog-title]");
    expect(title.exists()).toBeTruthy();

    const trigger = wrapper.find("div[data-test=dialog-trigger]");
    expect(trigger.exists()).toBeTruthy();

    const close = wrapper.find("button[data-test=dialog-close]");
    expect(close.exists()).toBeTruthy();
  });

  it("verify dialog attributes are hidden", async () => {
    const wrapper = createWrapper(DialogPanel, {
      propsData: {
        title: "Add New",
        value: false,
      },
    });

    const title = wrapper.find("div[data-test=dialog-title]");
    expect(title.exists()).toBeFalsy();

    const close = wrapper.find("button[data-test=dialog-close]");
    expect(close.exists()).toBeFalsy();
  });

  it("verify close icon click", async () => {
    const propsData = {
      title: "Add New",
      value: true,
      onClose: jest.fn(),
    };
    const wrapper = createWrapper(DialogPanel, { propsData });

    const close = wrapper.find("button[data-test=dialog-close]");
    expect(close.exists()).toBeTruthy();

    await close.trigger("click");
    expect(propsData.onClose).toHaveBeenCalled();
  });

  it("emits an input event when closed", async () => {
    const wrapper = createWrapper(DialogPanel, {
      propsData: {
        title: "Add New",
        value: true,
      },
    });

    const close = wrapper.find("button[data-test=dialog-close]");
    await close.trigger("click");

    expect(wrapper.emitted().input).toBeTruthy();
    expect(wrapper.emitted().input[0][0]).toBe(false);
  });
});
