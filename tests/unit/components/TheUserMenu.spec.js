import Vuex from "vuex";
import router from "@/router";
import TheUserMenu from "@/components/TheUserMenu";
import { createWrapper } from "../../test-wrapper";

jest.mock("@/router");

describe("TheUserMenu.vue", () => {
  function newWrapper(vuetify) {
    const options = {
      store,
      mocks: {
        $store: store,
        $router: router,
        $route: { name: "dashboard" },
      },
    };

    return createWrapper(TheUserMenu, options, vuetify);
  }

  let store;

  beforeEach(() => {
    router.push.mockReset();
    router.replace.mockReset();
  });

  it("displays the profile icon in the clipped square", () => {
    const state = {
      auth: { user: { firstName: "Tyrion", lastName: "Lannister" } },
    };
    store = new Vuex.Store({
      state,
    });

    const wrapper = newWrapper();
    const clippedSquare = wrapper.find("div[data-test=nav-bar-clipped-square]");
    expect(clippedSquare.exists()).toBeTruthy();
    expect(clippedSquare.rootNode.componentOptions.propsData).toHaveProperty(
      "color",
      "primary"
    );

    const profileIcon = wrapper.find("span[data-test=nav-bar-profile-icon");
    expect(profileIcon.exists()).toBeTruthy();
  });

  it("displays the profile icon in the accent color when selected", () => {
    const options = {
      store,
      mocks: {
        $store: store,
        $router: router,
        $route: { name: "profile" },
      },
    };

    const state = {
      auth: { user: { firstName: "Tyrion", lastName: "Lannister" } },
    };
    store = new Vuex.Store({
      state,
    });

    const wrapper = createWrapper(TheUserMenu, options);
    const clippedSquare = wrapper.find("div[data-test=nav-bar-clipped-square]");
    expect(clippedSquare.exists()).toBeTruthy();
    expect(clippedSquare.rootNode.componentOptions.propsData).toHaveProperty(
      "color",
      "accent"
    );
  });

  it("displays the profile icon on mobile navigation bottom bar", () => {
    const state = {
      auth: { user: { firstName: "Tyrion", lastName: "Lannister" } },
    };
    store = new Vuex.Store({
      state,
    });

    const wrapper = newWrapper({
      breakpoint: {
        init: jest.fn(),
        framework: {},
        smAndUp: false,
      },
    });

    const mobileNavBottomBarProfileIcon = wrapper.find(
      "[data-test=mobile-nav-bar-profile-icon]"
    );
    expect(mobileNavBottomBarProfileIcon.exists()).toBeTruthy();

    const clippedSquare = wrapper.find("div[data-test=nav-bar-profile-icon]");
    expect(clippedSquare.exists()).toBeFalsy();
  });
});
