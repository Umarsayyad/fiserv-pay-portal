import Legal from "@/views/legal/Legal";
import store from "@/store";
import { HIDE_FOOTER, SHOW_FOOTER } from "@/store/action-types";
import { createWrapper } from "../../../test-wrapper";

jest.mock("@/store");

function newWrapper() {
  return createWrapper(Legal, {
    mocks: {
      $store: store,
    },
  });
}

describe("Legal.vue", () => {
  it("dispatches hide footer beforeCreate", () => {
    newWrapper();
    expect(store.dispatch).toHaveBeenCalledWith(HIDE_FOOTER);
  });

  it("dispatches show footer on destroyed", () => {
    const wrapper = newWrapper();

    wrapper.destroy();

    expect(store.dispatch).toHaveBeenCalledWith(SHOW_FOOTER);
  });
});
