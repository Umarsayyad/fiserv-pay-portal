import TermsAndConditions from "@/views/legal/TermsAndConditions";
import { createWrapper } from "../../../test-wrapper";

describe("TermsAndConditions.vue", () => {
  it("exists", () => {
    const wrapper = createWrapper(TermsAndConditions);
    expect(wrapper.exists()).toBeTruthy();
  });
});
