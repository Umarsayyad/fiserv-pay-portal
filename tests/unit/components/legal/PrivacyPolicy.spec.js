import PrivacyPolicy from "@/views/legal/PrivacyPolicy";
import { createWrapper } from "../../../test-wrapper";

describe("PrivacyPolicy.vue", () => {
  it("exists", () => {
    const wrapper = createWrapper(PrivacyPolicy);
    expect(wrapper.exists()).toBeTruthy();
  });
});
