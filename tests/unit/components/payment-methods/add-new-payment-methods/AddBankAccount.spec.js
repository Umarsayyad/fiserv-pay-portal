import { createWrapper } from "../../../../test-wrapper";
import router from "@/router";
import AddBankAccount from "../../../../../src/components/payment-methods/add-new-payment-methods/AddBankAccount";
import Vuex from "vuex";
import paymentMethods from "@/store/payment-methods";
import { SET_DIALOG_CONTENT } from "@/store/mutation-types";

jest.mock("@/router");
jest.useFakeTimers();

describe("AddBankAccount.vue", () => {
  const store = new Vuex.Store({ modules: { paymentMethods } });
  store.commit = jest.fn();

  beforeEach(() => {
    router.push.mockReset();
    router.replace.mockReset();
  });

  it("displays add new bank account form", async () => {
    const wrapper = createWrapper(AddBankAccount, {
      propsData: {},
      mocks: {
        $router: router,
        $store: store,
      },
    });

    const infoSection = wrapper.findAll(
      "div[data-test=add-bank-account-info-section]"
    );
    expect(infoSection.exists()).toBeTruthy();

    const nameOfBankSection = wrapper.findAll(
      "input[data-test=add-bank-account-name-of-bank]"
    );
    expect(nameOfBankSection.exists()).toBeTruthy();

    const accountTypeSection = wrapper.findAll(
      "input[data-test=add-bank-account-account-type]"
    );
    expect(accountTypeSection.exists()).toBeTruthy();

    const accountNumberSection = wrapper.findAll(
      "input[data-test=add-bank-account-account-number]"
    );
    expect(accountNumberSection.exists()).toBeTruthy();

    const confirmAccountNumberSection = wrapper.findAll(
      "input[data-test=add-bank-account-confirm-account-number]"
    );
    expect(confirmAccountNumberSection.exists()).toBeTruthy();

    const routingNumberSection = wrapper.findAll(
      "input[data-test=add-bank-account-routing-number]"
    );
    expect(routingNumberSection.exists()).toBeTruthy();

    const routingNumberToolTip = wrapper.findAll(
      "div[data-test=add-bank-account-routing-number-tool-tip]"
    );
    expect(routingNumberToolTip.exists()).toBeTruthy();

    const termsAndConditionsSection = wrapper.findAll(
      "input[data-test=add-bank-account-terms-and-conditions]"
    );
    expect(termsAndConditionsSection.exists()).toBeTruthy();

    const makePrimaryMethodSection = wrapper.findAll(
      "div[data-test=add-bank-account-primary-method]"
    );
    expect(makePrimaryMethodSection.exists()).toBeTruthy();

    const addAccountBtn = wrapper.find(
      "button[data-test=add-bank-account-add-account]"
    );
    expect(addAccountBtn.exists()).toBeTruthy();
    expect(addAccountBtn.element.disabled).toBeTruthy();

    const cancelButton = wrapper.findAll(
      "button[data-test=add-bank-account-cancel]"
    );
    expect(cancelButton.exists()).toBeTruthy();
  });

  it("validate Name of the Bank Error Messages", async () => {
    const wrapper = createWrapper(AddBankAccount, {
      propsData: {},
      mocks: {
        $router: router,
        $store: store,
      },
    });

    const input = wrapper.find(
      "input[data-test=add-bank-account-name-of-bank]"
    );
    await input.trigger("focus");
    await input.trigger("blur");

    let errs = wrapper.find(".v-messages__message");
    expect(errs.text()).toBe("This is required field.");

    await input.setValue("secret");
    await input.trigger("input");

    errs = wrapper.find(".v-messages__message");
    expect(errs.exists()).toBeFalsy();
  });

  it("validate Account Number Error Messages", async () => {
    const wrapper = createWrapper(AddBankAccount, {
      propsData: {},
      mocks: {
        $router: router,
        $store: store,
      },
    });

    const input = wrapper.find(
      "input[data-test=add-bank-account-account-number]"
    );
    await input.trigger("focus");
    await input.trigger("blur");

    let errs = wrapper.find(".v-messages__message");
    expect(errs.text()).toBe("This is required field.");

    await input.setValue("secret");
    await input.trigger("input");

    errs = wrapper.find(".v-messages__message");
    expect(errs.exists()).toBeTruthy();
    expect(errs.text()).toBe("This field allows only numbers.");

    await input.setValue("123");
    await input.trigger("input");

    errs = wrapper.find(".v-messages__message");
    expect(errs.exists()).toBeFalsy();
  });

  it("validate Account Type Error Messages", async () => {
    const wrapper = createWrapper(AddBankAccount, {
      propsData: {},
      mocks: {
        $router: router,
        $store: store,
      },
    });

    const accountTypeSection = wrapper.find(
      "input[data-test=add-bank-account-account-type]"
    );

    await accountTypeSection.trigger("click");

    const options = wrapper.findAll("div.v-list-item");
    await options.at(1).trigger("click");

    let errs = wrapper.find(".v-messages__message");
    expect(errs.text()).toBe("This is required field.");
  });

  it("validate Confirm Account Number Error Messages", async () => {
    const wrapper = createWrapper(AddBankAccount, {
      propsData: {},
      mocks: {
        $router: router,
        $store: store,
      },
    });

    const input = wrapper.find(
      "input[data-test=add-bank-account-confirm-account-number]"
    );
    await input.trigger("focus");
    await input.trigger("blur");

    let errs = wrapper.find(".v-messages__message");
    expect(errs.text()).toBe("This is required field.");

    await input.setValue("secret");
    await input.trigger("input");

    errs = wrapper.find(".v-messages__message");
    expect(errs.exists()).toBeTruthy();
    expect(errs.text()).toBe("This field allows only numbers.");

    await input.setValue("123");
    await input.trigger("input");

    errs = wrapper.find(".v-messages__message");
    expect(errs.exists()).toBeTruthy();
    expect(errs.text()).toBe(
      "Account number and confirm account number fields doesn't match."
    );
  });

  it("validate Account Number and Confirm Account Number Match Validation", async () => {
    const wrapper = createWrapper(AddBankAccount, {
      propsData: {},
      mocks: {
        $router: router,
        $store: store,
      },
    });

    const accountNumberInput = wrapper.find(
      "input[data-test=add-bank-account-account-number]"
    );

    await accountNumberInput.setValue("123");
    await accountNumberInput.trigger("input");

    const confirmAccountNumberinput = wrapper.find(
      "input[data-test=add-bank-account-confirm-account-number]"
    );
    await confirmAccountNumberinput.setValue("123");
    await confirmAccountNumberinput.trigger("input");

    let errs = wrapper.find(".v-messages__message");
    errs = wrapper.find(".v-messages__message");
    expect(errs.exists()).toBeFalsy();
  });

  it("toggles the Account Number visibility when you click the eye icon", async () => {
    const wrapper = createWrapper(AddBankAccount, {
      propsData: {},
      mocks: {
        $router: router,
        $store: store,
      },
    });

    let input = wrapper.find(
      "input[data-test=add-bank-account-account-number]"
    );
    expect(input.element.type).toBe("password");

    await wrapper.find("button").trigger("click");
    input = wrapper.find("input");
    expect(input.element.type).toBe("text");
  });

  it("sets a 10sec timer when the eye-con is clicked", async () => {
    const wrapper = createWrapper(AddBankAccount, {
      propsData: {},
      mocks: {
        $router: router,
        $store: store,
      },
    });

    let input = wrapper.find(
      "input[data-test=add-bank-account-account-number]"
    );
    expect(input.element.type).toBe("password");

    await wrapper.find("button").trigger("click");
    expect(input.element.type).toBe("text");
    expect(setTimeout).toHaveBeenCalled();
    jest.advanceTimersByTime(10000);
    await wrapper.vm.$nextTick();

    input = wrapper.find("input[data-test=add-bank-account-account-number]");
    expect(input.element.type).toBe("password");

    // Validate the clear Timeout
    await wrapper.find("button").trigger("click");
    expect(input.element.type).toBe("text");

    await wrapper.find("button").trigger("click");
    expect(input.element.type).toBe("password");

    expect(clearTimeout).toHaveBeenCalled();
  });

  it("validate Routing Account Number Error Messages", async () => {
    const wrapper = createWrapper(AddBankAccount, {
      propsData: {},
      mocks: {
        $router: router,
        $store: store,
      },
    });

    const input = wrapper.find(
      "input[data-test=add-bank-account-routing-number]"
    );
    await input.trigger("focus");
    await input.trigger("blur");

    let errs = wrapper.find(".v-messages__message");
    expect(errs.text()).toBe("This is required field.");

    await input.setValue("secret");
    await input.trigger("input");

    errs = wrapper.find(".v-messages__message");
    expect(errs.exists()).toBeTruthy();
    expect(errs.text()).toBe("This field allows only numbers.");

    await input.setValue("123");
    await input.trigger("input");

    errs = wrapper.find(".v-messages__message");
    expect(errs.exists()).toBeFalsy();
  });

  it("validate Terms And Conditions Error Messages", async () => {
    const wrapper = createWrapper(AddBankAccount, {
      propsData: {},
      mocks: {
        $router: router,
        $store: store,
      },
    });

    const input = wrapper.find(
      "input[data-test=add-bank-account-terms-and-conditions]"
    );

    await input.trigger("focus");
    await input.trigger("blur");

    let errs = wrapper.find(".v-messages__message");
    expect(errs.text()).toBe("This is required field.");

    await input.setChecked();

    errs = wrapper.find(".v-messages__message");
    expect(errs.exists()).toBeFalsy();
  });

  it("verify Add Account Button validation", async () => {
    const wrapper = createWrapper(AddBankAccount, {
      propsData: {},
      mocks: {
        $router: router,
        $store: store,
      },
    });

    //check if disabled
    const addAccountBtn = wrapper.find(
      "button[data-test=add-bank-account-add-account]"
    );
    expect(addAccountBtn.exists()).toBeTruthy();
    expect(addAccountBtn.element.disabled).toBeTruthy();

    const nameOfBankSection = wrapper.findAll(
      "input[data-test=add-bank-account-name-of-bank]"
    );

    await nameOfBankSection.setValue("secret");
    await nameOfBankSection.trigger("input");

    const accountTypeSection = wrapper.find(
      "input[data-test=add-bank-account-account-type]"
    );

    await accountTypeSection.trigger("click");

    const options = wrapper.findAll("div.v-list-item");
    await options.at(1).trigger("click");

    const accountNumberSection = wrapper.findAll(
      "input[data-test=add-bank-account-account-number]"
    );

    await accountNumberSection.setValue("123");
    await accountNumberSection.trigger("input");

    const confirmAccountNumberSection = wrapper.findAll(
      "input[data-test=add-bank-account-confirm-account-number]"
    );

    await confirmAccountNumberSection.setValue("123");
    await confirmAccountNumberSection.trigger("input");

    const routingNumberSection = wrapper.findAll(
      "input[data-test=add-bank-account-routing-number]"
    );

    await routingNumberSection.setValue("123");
    await routingNumberSection.trigger("input");

    const termsAndConditionsSection = wrapper.findAll(
      "input[data-test=add-bank-account-terms-and-conditions]"
    );

    await termsAndConditionsSection.setChecked();

    //check if disabled
    expect(addAccountBtn.element.disabled).toBeFalsy();
  });

  it("verify cancel button", async () => {
    const wrapper = createWrapper(AddBankAccount, {
      propsData: {},
      mocks: {
        $router: router,
        $store: store,
      },
    });

    const cancelButton = wrapper.find(
      "button[data-test=add-bank-account-cancel]"
    );

    await cancelButton.trigger("click");
    expect(store.commit).toHaveBeenCalledWith(
      SET_DIALOG_CONTENT,
      "new-payout-methods"
    );
  });
});
