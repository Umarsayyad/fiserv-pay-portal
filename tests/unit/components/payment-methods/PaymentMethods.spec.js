import PaymentMethods from "@/components/payment-methods/PaymentMethods";
import { createWrapper } from "../../../test-wrapper";
import { mockPaymentMethods } from "@/store/payment-methods/mock-data";
import Vuex from "vuex";
import paymentMethods from "@/store/payment-methods";

describe("PaymentMethods.vue", () => {
  const loaderSelector = "div[data-test=payment-methods-loader]";
  const noPaymentMethodsSelector = "div[data-test=no-payment-methods]";

  const store = new Vuex.Store({ modules: { paymentMethods } });

  it("displays the loading indicators if loading", () => {
    const wrapper = createWrapper(PaymentMethods, {
      store,
      mocks: {
        $store: store,
      },
      propsData: {
        paymentMethods: mockPaymentMethods.accounts,
        loading: true,
      },
    });

    const loader = wrapper.find(loaderSelector);
    expect(loader.exists()).toBeTruthy();

    const noMethods = wrapper.find(noPaymentMethodsSelector);
    expect(noMethods.exists()).toBeFalsy();

    const methodsList = wrapper.find("div[data-test=payment-methods-list");
    expect(methodsList.exists()).toBeFalsy();
  });

  it("displays an indication that there are no payment methods", () => {
    const wrapper = createWrapper(PaymentMethods, {
      store,
      mocks: {
        $store: store,
      },
      propsData: {
        paymentMethods: [],
      },
    });

    const noMethods = wrapper.find(noPaymentMethodsSelector);
    expect(noMethods.exists()).toBeTruthy();

    const loader = wrapper.find(loaderSelector);
    expect(loader.exists()).toBeFalsy();

    const methodsList = wrapper.find("div[data-test=payment-methods-list");
    expect(methodsList.exists()).toBeFalsy();
  });

  it("displays the list of payment methods", () => {
    const wrapper = createWrapper(PaymentMethods, {
      store,
      mocks: {
        $store: store,
      },
      propsData: {
        paymentMethods: mockPaymentMethods.accounts,
      },
    });

    const methodsList = wrapper.find("div[data-test=payment-methods-list");
    expect(methodsList.exists()).toBeTruthy();

    const noMethods = wrapper.find(noPaymentMethodsSelector);
    expect(noMethods.exists()).toBeFalsy();

    const loader = wrapper.find(loaderSelector);
    expect(loader.exists()).toBeFalsy();
  });

  it("displays add new payment button", () => {
    const wrapper = createWrapper(PaymentMethods, {
      store,
      mocks: {
        $store: store,
      },
      propsData: {
        paymentMethods: mockPaymentMethods.accounts,
      },
    });
    const addNew = wrapper.find("button[data-test=add-new-dialog-trigger");
    expect(addNew.exists()).toBeTruthy();
  });

  it("does not display delimiters if there are 3 or less payments methods", () => {
    const wrapper = createWrapper(PaymentMethods, {
      store,
      mocks: {
        $store: store,
      },
      propsData: {
        paymentMethods: [
          {
            type: "ACH",
            name: "Bank of America",
            ach: {
              accountNumber: "123456789000",
              routingNumber: "283079227",
              type: "CHECKING",
            },
          },
        ],
      },
    });

    const delimiters = wrapper.find(
      "button[data-test=payment-method-delimiters"
    );
    expect(delimiters.exists()).toBeFalsy();
  });

  it("displays delimiters if there are more than 3 payments methods", () => {
    const wrapper = createWrapper(PaymentMethods, {
      store,
      mocks: {
        $store: store,
      },
      propsData: {
        paymentMethods: mockPaymentMethods.accounts,
      },
    });

    const delimiters = wrapper.find(
      "button[data-test=payment-method-delimiters"
    );
    expect(delimiters.exists()).toBeTruthy();
  });

  it("highlights second delimiter when clicked", async () => {
    const wrapper = createWrapper(PaymentMethods, {
      store,
      mocks: {
        $store: store,
      },
      propsData: {
        paymentMethods: mockPaymentMethods.accounts,
      },
    });

    const delimiters = wrapper.findAll(
      "button[data-test=payment-method-delimiters]"
    );

    await delimiters.at(1).trigger("click");

    expect(delimiters.at(1).classes()).toContain("accent");
  });

  it("resets the dialog", async () => {
    const wrapper = createWrapper(PaymentMethods, {
      store,
      mocks: {
        $store: store,
      },
      propsData: {
        paymentMethods: mockPaymentMethods.accounts,
      },
      data() {
        return {
          showDialog: true,
        };
      },
    });

    const bankAccountCard = wrapper.find(
      "div[data-test=new-payout-method-bank-account]"
    );
    await bankAccountCard.trigger("click");

    const bankAccountForm = wrapper.find(
      "div[data-test=add-bank-account-info-section]"
    );
    expect(bankAccountForm.isVisible()).toBeTruthy();

    const close = wrapper.find("button[data-test=dialog-close]");
    await close.trigger("click");

    const newPayoutMethods = wrapper.find(
      "div[data-test=new-payout-method-info-section]"
    );

    await wrapper
      .find("button[data-test=add-new-dialog-trigger]")
      .trigger("click");

    expect(newPayoutMethods.isVisible()).toBeTruthy();
    expect(bankAccountForm.isVisible()).toBeFalsy();
  });
});
