import { createWrapper } from "../../../test-wrapper";
import NewPayoutMethods from "../../../../src/components/payment-methods/NewPayoutMethods";

describe("NewPayoutMethods.vue", () => {
  it("displays new payout methods", async () => {
    const wrapper = createWrapper(NewPayoutMethods, {
      propsData: {},
    });

    const infoSection = wrapper.findAll(
      "div[data-test=new-payout-method-info-section]"
    );
    expect(infoSection.exists()).toBeTruthy();

    const bankAccount = wrapper.findAll(
      "div[data-test=new-payout-method-bank-account]"
    );
    expect(bankAccount.exists()).toBeTruthy();

    const creditCard = wrapper.findAll(
      "div[data-test=new-payout-method-credit-card]"
    );
    expect(creditCard.exists()).toBeTruthy();

    const venmo = wrapper.findAll("div[data-test=new-payout-method-venmo]");
    expect(venmo.exists()).toBeTruthy();

    const paypal = wrapper.findAll("div[data-test=new-payout-method-paypal]");
    expect(paypal.exists()).toBeTruthy();

    const moneyNetwork = wrapper.findAll(
      "div[data-test=new-payout-method-money-network]"
    );
    expect(moneyNetwork.exists()).toBeTruthy();
  });
});
