import { createWrapper } from "../../../test-wrapper";
import NewPayoutMethodCard from "../../../../src/components/payment-methods/NewPayoutMethodCard";

describe("NewPayoutMethodCard.vue", () => {
  function newWrapper(propsData) {
    return createWrapper(NewPayoutMethodCard, { propsData });
  }

  const mockData = {
    name: "Bank Account",
    timeFrame: "Instant",
  };

  it("displays the name of the payout method", () => {
    const wrapper = newWrapper({ payoutType: mockData, icon: "mdi-bank" });
    const name = wrapper.find("div[data-test=new-payout-type-name]");
    expect(name.element.textContent).toContain("Bank Account");
  });

  it("displays the time frame of the payout method", () => {
    const wrapper = newWrapper({ payoutType: mockData, icon: "mdi-bank" });
    const name = wrapper.find("div[data-test=new-payout-type-time-frame]");
    expect(name.element.textContent).toContain("Instant");
  });

  it("displays the icon of the payout method", () => {
    const wrapper = newWrapper({ payoutType: mockData, icon: "mdi-bank" });
    const name = wrapper.find("i[data-test=new-payout-icon]");
    expect(name.exists()).toBeTruthy();
  });

  it("displays the icon with brand logo of the payout method", () => {
    const wrapper = newWrapper({
      payoutType: {
        name: "Bank Account",
        timeFrame: "Instant",
        logo: "paypal",
      },
    });
    const name = wrapper.find("div[data-test=brand-logo-payout-icon]");
    expect(name.exists()).toBeTruthy();
  });

  it("displays the review all", () => {
    const wrapper = newWrapper({ payoutType: mockData, icon: "mdi-bank" });
    const name = wrapper.find("div[data-test=new-payout-review-all]");
    expect(name.exists()).toBeTruthy();
  });

  it("hover through elevation", async () => {
    const wrapper = newWrapper({ payoutType: mockData, icon: "mdi-bank" });
    let classes = wrapper.classes();
    expect(classes).toContain("elevation-0");
    expect(classes).not.toContain("elevation-12");

    await wrapper.vm.$nextTick();
    requestAnimationFrame(() => {
      let classes = wrapper.classes();
      expect(classes).toContain("elevation-12");
      expect(classes).not.toContain("elevation-0");
    });
  });
});
