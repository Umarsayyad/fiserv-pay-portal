import PaymentMethodInfoCard from "@/components/payment-methods/PaymentMethodInfoCard";
import { createWrapper } from "../../../test-wrapper";
import { mockPaymentMethods } from "@/store/payment-methods/mock-data";

describe("PaymentMethodInfoCard.vue", () => {
  function newWrapper(propsData) {
    return createWrapper(PaymentMethodInfoCard, { propsData });
  }

  const mockDebit = mockPaymentMethods.accounts[0];
  const mockChecking = mockPaymentMethods.accounts[1];

  it("displays the name of the payment method ", () => {
    const wrapper = newWrapper({ paymentMethod: mockDebit });
    const name = wrapper.find("div[data-test=payment-name]");
    expect(name.element.textContent).toContain("CitiBank");
  });

  it("renders the debit container when payment method type is debit ", () => {
    const wrapper = newWrapper({ paymentMethod: mockDebit });
    const debit = wrapper.find("div[data-test=debit-container]");
    expect(debit.exists()).toBeTruthy();
  });

  it("does not render the debit container when payment method type is not debit ", () => {
    const wrapper = newWrapper({ paymentMethod: mockChecking });
    const debit = wrapper.find("div[data-test=debit-container]");
    expect(debit.exists()).toBeFalsy();
  });

  it("renders the checking container when payment method type is checking ", () => {
    const wrapper = newWrapper({ paymentMethod: mockChecking });
    const checking = wrapper.find("div[data-test=checking-container]");
    expect(checking.exists()).toBeTruthy();
  });

  it("does not render the checking container when payment method type is not checking ", () => {
    const wrapper = newWrapper({ paymentMethod: mockDebit });
    const checking = wrapper.find("div[data-test=checking-container]");
    expect(checking.exists()).toBeFalsy();
  });
});
