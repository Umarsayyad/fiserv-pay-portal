import Vuex from "vuex";
import { createWrapper } from "../../test-wrapper";
import TheErrorAlert from "@/components/TheErrorAlert";
import appState from "@/store/app/state";
import mutations from "@/store/app/mutations";
import { CLEAR_APP_ERRORS } from "@/store/mutation-types";

describe("TheErrorAlert.vue", () => {
  let store;

  function newWrapper(err) {
    const app = appState();

    if (err) {
      app.error = err;
    }

    const state = { app };
    store = new Vuex.Store({
      state,
      mutations,
    });

    jest.spyOn(store, "commit");

    return createWrapper(TheErrorAlert, { store });
  }

  it("is hidden when app.error.show = false", () => {
    const wrapper = newWrapper();
    expect(wrapper.isVisible()).toBeFalsy();
  });

  it("is visible when app.error.show = true", () => {
    const wrapper = newWrapper({ show: true, values: [] });
    expect(wrapper.isVisible()).toBeTruthy();
  });

  it("displays the list of errors", () => {
    const values = ["one err", "two err"];

    const wrapper = newWrapper({ show: true, values: values });
    const errs = wrapper.findAll("li[data-test=the-error-alert-list-item]");

    expect(errs.length).toBe(values.length);

    for (let i = 0; i < errs.length; i++) {
      expect(errs.at(i).text()).toBe(values[i]);
    }
  });

  it("clears the errors when you dismiss the alert", async () => {
    const values = ["one err", "two err"];
    const wrapper = newWrapper({ show: true, values: values });

    await wrapper.find("button").trigger("click");

    expect(store.commit).toHaveBeenCalledWith(CLEAR_APP_ERRORS);
  });
});
