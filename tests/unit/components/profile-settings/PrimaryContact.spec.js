import { createWrapper } from "../../../test-wrapper";
import { mockUser } from "@/store/profile-settings/mock-data";
import PrimaryContact from "@/components/profile-settings/PrimaryContact.vue";

describe("PrimaryContact.vue", () => {
  function newWrapper(propsData) {
    return createWrapper(PrimaryContact, { propsData });
  }

  it("displays the users full name", () => {
    const wrapper = newWrapper({ user: mockUser });
    const name = wrapper.find("input[data-test=name]");
    expect(name.element.value).toBe("Tyrion Lannister");
  });

  it("displays the users date of birth", () => {
    const wrapper = newWrapper({ user: mockUser });
    const dob = wrapper.find("input[data-test=dob]");
    expect(dob.element.value).toBe("11/11/1981");
  });

  it("displays the users tin", () => {
    mockUser.dateOfBirth = "tin";
    delete mockUser.dateOfBirth;
    mockUser.tin = "11111981";
    const wrapper = newWrapper({ user: mockUser });
    const tin = wrapper.find("input[data-test=tin]");
    expect(tin.element.value).toBe("11111981");
  });

  it("displays the users address", () => {
    const wrapper = newWrapper({ user: mockUser });
    const address = wrapper.find("input[data-test=address]");
    expect(address.element.value).toBe(
      "1255 Corporate Drive, Irving, CA 91608 USA"
    );
  });

  it("displays the users phone number", () => {
    const wrapper = newWrapper({ user: mockUser });
    const name = wrapper.find("input[data-test=phone-number]");
    expect(name.element.value).toBe("123-456-7890");
  });

  it("displays the users username", () => {
    const wrapper = newWrapper({ user: mockUser });
    const username = wrapper.find("input[data-test=username]");
    expect(username.element.value).toBe("tyrion.lannister");
  });
});
