import { createWrapper } from "../../../test-wrapper";
import { mockUser } from "@/store/profile-settings/mock-data";
import EmailSettings from "@/components/profile-settings/EmailSettings.vue";

describe("EmailSettings.vue", () => {
  function newWrapper(propsData) {
    return createWrapper(EmailSettings, { propsData });
  }
  it("displays the users email address", () => {
    const wrapper = newWrapper({ user: mockUser });
    const email = wrapper.find("div[data-test=email-address]");
    expect(email.element.textContent.trim()).toContain(
      "tyrion@lannister.house"
    );
  });
});
