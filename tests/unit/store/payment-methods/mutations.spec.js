import mutations from "@/store/payment-methods/mutations";

describe("payment-methods/mutations", () => {
  it("sets the payout", () => {
    const state = { dialogContent: null };
    const dialogContent = "some_content";

    mutations.SET_DIALOG_CONTENT(state, dialogContent);

    expect(state.dialogContent).toEqual(dialogContent);
  });
});
