import paymentMethods from "@/store/payment-methods";
import { mockPaymentMethods } from "@/store/payment-methods/mock-data";

const { actions } = paymentMethods;

describe("payment-methods/actions", () => {
  it("fetches the payment methods", async () => {
    const data = await actions.FETCH_PAYMENT_METHODS({
      state: { paymentMethods: mockPaymentMethods },
    });
    expect(data).toEqual(mockPaymentMethods.accounts);
  });
});
