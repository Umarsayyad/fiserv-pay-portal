import app from "@/store/app";
import { SHOW_FOOTER, HIDE_FOOTER } from "@/store/action-types";

const { actions } = app;
describe("app/actions", () => {
  it("uses the action to set the mutation for the footer to be true", () => {
    let data;
    const commit = (state, payload) => (data = payload);
    actions[SHOW_FOOTER]({ commit });
    expect(data).toBeTruthy();
  });
  it("uses the action to set the mutation for the footer to be false", () => {
    let data;
    const commit = (state, payload) => (data = payload);
    actions[HIDE_FOOTER]({ commit });
    expect(data).toBeFalsy();
  });
});
