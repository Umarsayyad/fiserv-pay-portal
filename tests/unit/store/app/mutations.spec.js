import {
  CLEAR_APP_ERRORS,
  PUSH_APP_ERROR,
  SET_SHOW_FOOTER,
  SET_SPLIT_BACKGROUND,
} from "@/store/mutation-types";
import app from "@/store/app";

const { mutations } = app;

describe("app/mutations", () => {
  it("sets the state of the footer", () => {
    const state = { showFooter: true };

    mutations[SET_SHOW_FOOTER](state, false);

    expect(state.showFooter).toBe(false);
  });

  it("sets splitBackground", () => {
    const state = { splitBackground: true };

    mutations[SET_SPLIT_BACKGROUND](state, false);

    expect(state.splitBackground).toBe(false);
  });

  it("pushes an app error", () => {
    const state = { error: { show: false, values: [] } };

    const err = "Something bad happened";
    mutations[PUSH_APP_ERROR](state, err);

    expect(state.error).toEqual({ show: true, values: [err] });
  });

  it("clears the app errors", () => {
    const state = { error: { show: true, values: ["one err", "two err"] } };

    mutations[CLEAR_APP_ERRORS](state);

    expect(state.error.show).toBeFalsy();
    expect(state.error.values.length).toBe(0);
  });
});
