import app from "@/store/app";

const { state } = app;

describe("app/state", () => {
  it("has state", () => {
    const actual = state();
    expect(actual).toBeTruthy();
  });
});
