import getters from "@/store/auth/getters";

describe("auth/getters", () => {
  it("returns isAuthenticated = true when there's a valid session", () => {
    const state = {
      user: {
        signInUserSession: {
          idToken: {
            jwtToken: "jwt",
          },
        },
      },
    };
    const isAuthenticated = getters.isAuthenticated(state);
    expect(isAuthenticated).toBe(true);
  });

  it("returns isAuthenticated = false when there's no user", () => {
    const state = {
      user: null,
    };
    const isAuthenticated = getters.isAuthenticated(state);
    expect(isAuthenticated).toBe(false);
  });
});
