import { Auth } from "aws-amplify";
import auth from "@/store/auth";
import {
  COMPLETE_NEW_PASSWORD,
  CONFIRM_SIGN_IN,
  CURRENT_AUTHENTICATED_USER,
  DISABLE_SMS_MFA,
  ENABLE_SMS_MFA,
  SIGN_IN,
  SIGN_OUT,
  FORGOT_PASSWORD,
  FORGOT_PASSWORD_SUBMIT,
} from "@/store/action-types";
import { SET_REDIRECT, SET_USER } from "@/store/mutation-types";

const { actions } = auth;

jest.mock("aws-amplify");

describe("auth/actions", () => {
  beforeEach(() => {
    Auth.setPreferredMFA.mockReset();
  });

  it("signs in", () => {
    const user = {
      username: "tyrion.lannister",
      email: "tyrion@lannister.house",
    };
    Auth.signIn.mockResolvedValue(user);

    let data;
    const commit = (state, payload) => (data = payload);

    const creds = { username: user.username, password: "secret" };
    actions[SIGN_IN]({ commit }, creds).then(() => {
      expect(Auth.signIn).toBeCalledWith(creds.username, creds.password);
      expect(data).toEqual(user);
    });
  });

  it("completes new password", () => {
    const user = {
      username: "tyrion.lannister",
      email: "tyrion@lannister.house",
    };
    Auth.completeNewPassword.mockResolvedValue(user);

    let data;
    const commit = (state, payload) => (data = payload);
    const state = { user: user };

    const newPassword = "secret";
    actions[COMPLETE_NEW_PASSWORD]({ commit, state }, newPassword).then(() => {
      expect(Auth.completeNewPassword).toBeCalledWith(user, newPassword);
      expect(data).toEqual(user);
    });
  });

  it("signs out", () => {
    Auth.signOut.mockResolvedValue(undefined);

    let user = { username: "tyrion.lannister" };
    let redirect = null;
    const commit = (mutation, payload) => {
      if (mutation === SET_USER) {
        user = payload;
      }

      if (mutation === SET_REDIRECT) {
        redirect = payload;
      }
    };

    actions[SIGN_OUT]({ commit }).then(() => {
      expect(Auth.signOut).toHaveBeenCalled();
      expect(user).toBeNull();
      expect(redirect).toEqual({ name: "home" });
    });
  });

  it("gets the current authenticated user", () => {
    const user = {
      username: "tyrion.lannister",
      email: "tyrion@lannister.house",
    };
    Auth.currentAuthenticatedUser.mockResolvedValue(user);

    let data;
    const commit = (state, payload) => (data = payload);

    actions[CURRENT_AUTHENTICATED_USER]({ commit }).then(() => {
      expect(Auth.currentAuthenticatedUser).toHaveBeenCalled();
      expect(data).toEqual(user);
    });
  });

  it("uses username to send otp to change password", () => {
    const user = { username: "tyrion.lannister" };

    Auth.forgotPassword.mockResolvedValue(undefined);

    actions[FORGOT_PASSWORD](null, user.username).then(() => {
      expect(Auth.forgotPassword).toBeCalledWith(user.username);
    });
  });

  it("resets password with otp and new password", () => {
    const user = {
      username: "tyrion.lannister",
      otp: "123456",
      newPassword: "fiserv123",
    };

    Auth.forgotPasswordSubmit.mockResolvedValue(undefined);

    actions[FORGOT_PASSWORD_SUBMIT](null, user).then(() => {
      expect(Auth.forgotPasswordSubmit).toBeCalledWith(
        user.username,
        user.otp,
        user.newPassword
      );
    });
  });
  it("confirms sign in", () => {
    const user = {
      username: "tyrion.lannister",
      email: "tyrion@lannister.house",
    };
    Auth.confirmSignIn.mockResolvedValue(user);

    let data;
    const commit = (state, payload) => (data = payload);
    const state = { user: user };

    const code = "123456";
    actions[CONFIRM_SIGN_IN]({ commit, state }, code).then(() => {
      expect(Auth.confirmSignIn).toBeCalledWith(user, code);
      expect(data).toEqual(user);
    });
  });

  it("enables mfa", async () => {
    Auth.setPreferredMFA.mockResolvedValue(undefined);

    const state = { user: { preferredMFA: "NOMFA" } };
    await actions[ENABLE_SMS_MFA]({ state });

    expect(Auth.setPreferredMFA).toHaveBeenCalledWith(state.user, "SMS");
  });

  it("disables mfa", async () => {
    Auth.setPreferredMFA.mockResolvedValue(undefined);

    const state = { user: { preferredMFA: "SMS_MFA" } };
    await actions[DISABLE_SMS_MFA]({ state });

    expect(Auth.setPreferredMFA).toHaveBeenCalledWith(state.user, "NOMFA");
  });
});
