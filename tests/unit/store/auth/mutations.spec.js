import { SET_REDIRECT, SET_USER } from "@/store/mutation-types";
import auth from "@/store/auth";

const { mutations } = auth;

describe("auth/mutations", () => {
  it("sets the user", () => {
    const state = { user: null };
    const user = {
      username: "tyrion.lannister",
      email: "tyrion@lannister.house",
    };

    mutations[SET_USER](state, user);

    expect(state.user).toEqual(user);
  });

  it("sets the redirect", () => {
    const state = { redirect: null };
    const redirect = { name: "payouts" };

    mutations[SET_REDIRECT](state, redirect);

    expect(state.redirect).toEqual(redirect);
  });
});
