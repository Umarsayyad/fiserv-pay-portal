import auth from "@/store/auth";

const { state } = auth;

describe("auth/state", () => {
  it("has state", () => {
    const actual = state();
    expect(actual).toBeTruthy();
  });
});
