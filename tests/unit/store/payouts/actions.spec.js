import payouts from "@/store/payouts";
import { SET_PAYOUT } from "@/store/mutation-types";
import {
  mockAvailablePayouts,
  mockPayoutHistory,
  mockPayouts,
  mockTotalPayoutAmountsByMerchant,
} from "@/store/payouts/mock-data";
import {
  FETCH_PAYOUT_HISTORY,
  FETCH_TOTAL_PAYOUT_AMOUNTS_BY_MERCHANT,
} from "@/store/action-types";

const { actions } = payouts;

describe("payouts/actions", () => {
  it("returns any empty array of payouts", async () => {
    const data = await actions.FETCH_PAYOUTS({}, true);
    expect(data.transactions).toEqual([]);
  });

  it("fetches the payouts", async () => {
    const data = await actions.FETCH_PAYOUTS(
      { state: { payouts: mockPayouts } },
      false
    );
    expect(data).toEqual(mockPayouts);
  });

  it("fetches a payout", async () => {
    const commit = jest.fn();
    const state = { payouts: mockPayouts };

    const id = "TID-eb59f713-edf4-48ba-8f52-1379df1db9b3";
    const data = await actions.FETCH_PAYOUT({ commit, state }, id);
    expect(data.transactionId).toBe(id);
    expect(commit).toHaveBeenCalledWith(SET_PAYOUT, data);
  });

  // todo: remove later. this test is temporary while we're mocking api calls
  it("returns a 404 when there's no payout", async () => {
    const commit = jest.fn();
    const state = { payouts: mockPayouts };

    const id = "bad id";
    try {
      await actions.FETCH_PAYOUT({ commit, state }, id);
    } catch (err) {
      expect(commit).not.toHaveBeenCalled();
      expect(err.response.status).toBe(404);
    }
  });

  it("rejects a payout", async () => {
    const payout = { ...mockPayouts.transactions[0] };
    const state = { payouts: mockPayouts, payout };
    const commit = (mutation, payload) => {
      if (mutation === SET_PAYOUT) {
        state.payout = payload;
      } else {
        state.payouts = payload;
      }
    };

    const description = "I'd rather have a paper check...";
    await actions.REJECT_PAYOUT({ commit, state }, description);
    expect(state.payout.transactionStatus).toBe("TC");
    expect(state.payout.reason).toBe(description);

    expect(state.payouts.transactions[0].transactionStatus).toBe("TC");
    expect(state.payouts.transactions[0].reason).toBe(description);
  });

  it("fetches the available payouts", async () => {
    const state = { availablePayouts: mockAvailablePayouts };
    const data = await actions.FETCH_AVAILABLE_PAYOUTS({ state });

    expect(data).toEqual(mockAvailablePayouts);
  });

  it("fetches the total payout amounts by merchant", async () => {
    const state = {
      totalPayoutAmountsByMerchant: mockTotalPayoutAmountsByMerchant,
    };
    const data = await actions[FETCH_TOTAL_PAYOUT_AMOUNTS_BY_MERCHANT]({
      state,
    });

    expect(data).toEqual(mockTotalPayoutAmountsByMerchant);
  });

  it("fetches the payout history", async () => {
    const state = { history: mockPayoutHistory };

    const data = await actions[FETCH_PAYOUT_HISTORY](
      { state },
      { size: 10, page: 1 }
    );

    expect(data).toEqual({
      transactions: mockPayoutHistory.transactions.slice(0, 10),
      pages: mockPayoutHistory.pages,
    });
  });
});
