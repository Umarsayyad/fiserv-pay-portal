import mutations from "@/store/payouts/mutations";

describe("payouts/mutations", () => {
  it("sets the payout", () => {
    const state = { payout: null };
    const payout = { transactionId: "123" };

    mutations.SET_PAYOUT(state, payout);

    expect(state.payout).toEqual(payout);
  });

  it("sets the payouts", () => {
    const state = { payouts: null };
    const payouts = { transactions: [{ transactionId: "1" }] };

    mutations.SET_PAYOUTS(state, payouts);

    expect(state.payouts).toEqual(payouts);
  });
});
