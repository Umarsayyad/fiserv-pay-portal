import getters from "@/store/payouts/getters";
import { add, format } from "date-fns";

const now = new Date();
const dateFmt = "MM/dd/yyyy";
const nowFormatted = format(now, dateFmt);

const tomorrow = add(now, { days: 1 });
const tomorrowFormatted = format(tomorrow, dateFmt);

describe("payouts/getters", () => {
  it("gets the brand from a payout", () => {
    const state = { payout };
    const brand = getters.brand(state);
    expect(brand).toEqual({
      groupName: "statefarm",
      displayName: "State Farm",
    });
  });

  it("returns null when there is no payout", () => {
    const state = {};
    const brand = getters.brand(state);
    expect(brand).toBeNull();
  });

  it("gets sum of transactions ", () => {
    let state = { availablePayouts: mockAvailablePayouts };
    let total = getters.availablePayoutAmount(state);
    expect(total).toBe(701);

    state = {
      availablePayouts: { ...mockAvailablePayouts, transactions: null },
    };
    total = getters.availablePayoutAmount(state);
    expect(total).toBe(0);
  });
});

const mockAvailablePayouts = {
  transactions: [
    {
      transactionId: "TID-f142f499-d86f-4364-920e-7d2de3900b9c",
      created: 1618592828,
      merchantTransactionId: "TransactionID-1991309",
      paymentReferenceNumber: "1234567904",
      applyDate: "04/15/2021",
      transactionDate: "04/15/2021",
      updateDate: "04/15/2021",
      expiryDate: tomorrowFormatted,
      recipient: [
        {
          merchantCustomerId: "merchantCustomerId-29764558",
          recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
          payments: {
            paymentId: "24e2e932-5f14-44c0-ace8-a03ea5260349",
            amount: {
              total: 200,
              currency: "USD",
            },
            fee: {
              total: 0,
              currency: "USD",
            },
            paymentType: "Claims",
            paymentStatus: "DP",
          },
          doingBusinessAs: "Lyft",
          firstName: "TYRION",
          lastName: "LANNISTER",
          groupName: "lyft",
        },
      ],
      transactionStatus: "PD",
      totalTransactionAmount: {
        total: 200,
        currency: "USD",
      },
    },
    {
      transactionId: "TID-ee1f4255-3c2e-45b6-8028-5c2b587fde4c",
      created: 1618592828,
      merchantTransactionId: "TransactionID-1991310",
      paymentReferenceNumber: "1234567904",
      applyDate: "04/16/2021",
      transactionDate: "04/16/2021",
      updateDate: "04/16/2021",
      expiryDate: nowFormatted,
      recipient: [
        {
          merchantCustomerId: "merchantCustomerId-29764558",
          recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
          payments: {
            paymentId: "47884808-337f-4a19-83d1-5cb6a16189a7",
            amount: {
              total: 201,
              currency: "USD",
            },
            fee: {
              total: 0,
              currency: "USD",
            },
            paymentType: "Claims",
            paymentStatus: "DP",
          },
          doingBusinessAs: "State Farm",
          firstName: "TYRION",
          lastName: "LANNISTER",
          groupName: "statefarm",
        },
      ],
      transactionStatus: "PD",
      totalTransactionAmount: {
        total: 201,
        currency: "USD",
      },
    },
    {
      transactionId: "TID-094d7fc0-2f81-4fb7-8ca7-f2683c382e45",
      created: 1618592828,
      merchantTransactionId: "merchantTransactionId-098699",
      applyDate: "04/08/2021",
      transactionDate: "04/08/2021",
      updateDate: "04/11/2021",
      expiryDate: tomorrowFormatted,
      recipient: [
        {
          merchantCustomerId: "merchantCustomerId-29764558",
          recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
          payments: {
            paymentId: "fa81b013-aed7-490d-950c-3290139ecbdd",
            amount: {
              total: 100,
              currency: "USD",
            },
            fee: {
              total: 0,
              currency: "USD",
            },
            paymentType: "Claims",
            paymentStatus: "PE",
          },
          doingBusinessAs: "Lyft",
          firstName: "TYRION",
          lastName: "LANNISTER",
          groupName: "lyft",
        },
      ],
      transactionStatus: "PD",
      totalTransactionAmount: {
        total: 100,
        currency: "USD",
      },
    },
    {
      transactionId: "TID-eb59f713-edf4-48ba-8f52-1379df1db9b3",
      created: 1618592828,
      merchantTransactionId: "merchantTransactionId-098698",
      applyDate: "04/08/2021",
      transactionDate: "04/08/2021",
      updateDate: "04/11/2021",
      expiryDate: tomorrowFormatted,
      recipient: [
        {
          merchantCustomerId: "merchantCustomerId-29764558",
          recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
          payments: {
            paymentId: "e35c34d2-e851-47db-befd-deb37f53041e",
            amount: {
              total: 100,
              currency: "USD",
            },
            fee: {
              total: 0,
              currency: "USD",
            },
            paymentType: "Claims",
            paymentStatus: "PE",
          },
          doingBusinessAs: "State Farm",
          firstName: "TYRION",
          lastName: "LANNISTER",
          groupName: "statefarm",
        },
      ],
      transactionStatus: "PD",
      totalTransactionAmount: {
        total: 100,
        currency: "USD",
      },
    },
    {
      transactionId: "TID-094d7fc0-2f81-4fb7-8ca7-f2683c382e45",
      created: 1618592828,
      merchantTransactionId: "merchantTransactionId-098699",
      applyDate: "04/08/2021",
      transactionDate: "04/08/2021",
      updateDate: "04/11/2021",
      expiryDate: tomorrowFormatted,
      recipient: [
        {
          merchantCustomerId: "merchantCustomerId-29764558",
          recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
          payments: {
            paymentId: "fa81b013-aed7-490d-950c-3290139ecbdd",
            amount: {
              total: 100,
              currency: "USD",
            },
            fee: {
              total: 0,
              currency: "USD",
            },
            paymentType: "Claims",
            paymentStatus: "PE",
          },
          doingBusinessAs: "State Farm",
          firstName: "TYRION",
          lastName: "LANNISTER",
          groupName: "statefarm",
        },
      ],
      transactionStatus: "PD",
      totalTransactionAmount: {
        total: 100,
        currency: "USD",
      },
    },
  ],
  pages: {
    firstPage: true,
    lastPage: true,
    currentPage: 1,
    currentPageRecords: 5,
    totalPages: 1,
    totalRecords: 5,
    sortOrder: "desc",
  },
};

const payout = {
  transactionId: "TID-ee1f4255-3c2e-45b6-8028-5c2b587fde4c",
  created: 1618592828,
  merchantTransactionId: "TransactionID-1991310",
  paymentReferenceNumber: "1234567904",
  applyDate: "04/16/2021",
  transactionDate: "04/16/2021",
  updateDate: "04/16/2021",
  recipient: [
    {
      merchantCustomerId: "merchantCustomerId-29764558",
      recipientId: "8a7fd6a1786d0dc20178b29df00e1db0",
      payments: {
        paymentId: "47884808-337f-4a19-83d1-5cb6a16189a7",
        amount: {
          total: 201,
          currency: "USD",
        },
        fee: {
          total: 0,
          currency: "USD",
        },
        paymentType: "Claims",
        paymentStatus: "DP",
      },
      doingBusinessAs: "State Farm",
      firstName: "TYRION",
      lastName: "LANNISTER",
      groupName: "statefarm",
    },
  ],
  transactionStatus: "PD",
  totalTransactionAmount: {
    total: 201,
    currency: "USD",
  },
};
