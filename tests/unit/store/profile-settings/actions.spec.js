import profile from "@/store/profile-settings";
import { mockUser } from "@/store/profile-settings/mock-data";

const { actions } = profile;

describe("payment-methods/actions", () => {
  it("fetches the user settings", async () => {
    const data = await actions.FETCH_USER_SETTINGS({
      state: { profile: mockUser },
    });
    expect(data).toEqual(mockUser);
  });
});
