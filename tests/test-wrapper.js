import { createLocalVue, mount } from "@vue/test-utils";
import Vuex from "vuex";
import { Vuelidate } from "vuelidate";
import Vuetify from "vuetify";
import { i18n } from "@/plugins/i18n";
import {
  capitalize,
  currency,
  shortDate,
  maskAccountNumber,
  phoneNumber,
} from "@/filters/filters";

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(Vuelidate);
localVue.filter("longDate", () => {});
localVue.filter("shortDate", shortDate);
localVue.filter("currency", currency);
localVue.filter("capitalize", capitalize);
localVue.filter("maskAccountNumber", maskAccountNumber);
localVue.filter("phoneNumber", phoneNumber);

const vuetify = new Vuetify();

export const createWrapper = (component, options, vuetifyServices) => {
  document.body.setAttribute("data-app", "true");
  const attachTo = document.createElement("div");
  if (document.body) {
    document.body.appendChild(attachTo);
  }

  if (vuetifyServices) {
    vuetify.framework = { ...vuetify.framework, ...vuetifyServices };
  }

  let opts = {
    localVue,
    vuetify,
    attachTo,
    stubs: ["router-link", "router-view"],
    i18n,
  };

  if (options) {
    opts = { ...opts, ...options };
  }

  return mount(component, opts);
};
