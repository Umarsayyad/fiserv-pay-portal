import Vue from "vue";
import Vuetify from "vuetify";

Vue.config.productionTip = false;
Vue.config.devtools = false;

Vue.use(Vuetify);

class ResizeObserver {
  observe() {
    // do nothing
  }
  unobserve() {
    // do nothing
  }
  disconnect() {
    // do nothing
  }
}

window.ResizeObserver = ResizeObserver;
